
# Project Overview

This piece of software is the client part of the spendingtracker system developed for tracking running expenses in order to answer to the following questions:
  + Are you are spending more than expected?
  + Are you are spending faster than expected?

# Installation and Running Instructions

Please go through the following instructions in order to set up the development environment.

## Installation of Dependencies

~~~
npm install
~~~

## Running of Application

Navigate to project root folder and then...
~~~
npm start
~~~

Open the browser and navigate to...
http://localhost:3000/

Default credentials:
+ Username: admin
+ Password: password

*NB! Make sure you have the spendingtracker API set up and running in your localhost before starting up this client part of the software.*
