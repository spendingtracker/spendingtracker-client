import React, { Component } from "react";
import { connect } from "react-redux";
import Selectors from "../redux/selectors";
import Actions from "../redux/actions";

import { IoMdMenu, IoMdClose } from "react-icons/io";

class MenuButton extends Component {
  render() {
    const icon = this.props.isMenuOpen ? <IoMdClose /> : <IoMdMenu />;
    return (
      <div className="menu-button" onClick={this.props.toggleMenu}>
        {icon}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isMenuOpen: Selectors.isMenuOpen(state)
});

const mapDispatchToProps = dispatch => ({
  toggleMenu: () => dispatch(Actions.toggleMenu())
});

export default connect(mapStateToProps, mapDispatchToProps)(MenuButton);
