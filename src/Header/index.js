import React, { Component } from "react";
import { connect } from "react-redux";
import Actions from "../redux/actions";
import Selectors from "../redux/selectors";
import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";
import MenuButton from "./MenuButton";
import Helper from "../Util/Helper";

class Header extends Component {
  render() {
    return (
      <div className="text-center">
        <table width="100%">
          <tbody>
            <tr>
              <td width="1%">
                <div className="menu-button" />
              </td>
              <td width="98%">
                <div className="headline-1">
                  <IoIosArrowBack
                    onClick={this.props.decreaseMonthOffset}
                    className="icon-link"
                  />{" "}
                  {Helper.getMonthStr(this.props.monthOffset)}{" "}
                  <IoIosArrowForward
                    onClick={this.props.increaseMonthOffset}
                    className="icon-link"
                  />
                </div>
              </td>
              <td width="1%">
                <MenuButton />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  monthOffset: Selectors.getMonthOffset(state)
});

const mapDispatchToProps = dispatch => ({
  increaseMonthOffset: () => dispatch(Actions.increaseMonthOffset()),
  decreaseMonthOffset: () => dispatch(Actions.decreaseMonthOffset())
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
