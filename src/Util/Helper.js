import moment from "moment";

const Helper = {
  removeEuroSymbolFromPriceSubstr(priceCandidate) {
    return priceCandidate.replace(/^€\s*/, "");
  },

  isPriceSubstr(priceCandidate) {
    return (
      priceCandidate === "" ||
      priceCandidate.match(/^[0-9]{1,4}([.][0-9]{0,2})?$/)
    );
  },

  getMonthStr(monthOffset) {
    return moment().add(monthOffset, "month").format("MMM YYYY");
  },

  toMonthStr(momentDateTime) {
    return momentDateTime.format("MMM YYYY");
  },

  getPastPeriods(monthOffset) {
    let pastPeriods = [];
    let offsetMonth = moment().add(monthOffset, "month");
    for (let i = -1; i >= -24; i--) {
      offsetMonth.add(-1, "month");
      pastPeriods.push({
        year: offsetMonth.year(),
        month: offsetMonth.month() + 1,
        text: this.toMonthStr(offsetMonth),
        monthOffset: monthOffset + i,
      });
    }
    return pastPeriods;
  },

  getOffsetPeriod(monthOffset) {
    const offsetMonthStart = moment().add(monthOffset, "month");
    const offsetMonthEnd = offsetMonthStart.clone().add(1, "month");
    return {
      year: offsetMonthStart.year(),
      month: offsetMonthStart.month() + 1,
      days: offsetMonthEnd.diff(offsetMonthStart, "days"),
      periodStart: moment([
        offsetMonthStart.year(),
        offsetMonthStart.month(),
        1,
      ]),
    };
  },

  formatTime_DDMMYYYYHHMM(time) {
    return time != null ? moment(time).format("DD.MM.YYYY HH:mm") : null;
  },

  formatTime_DDMMYYYY(time) {
    return time != null ? moment(time).format("DD.MM.YYYY") : null;
  },

  formatTime_HHMM(time) {
    return time != null ? moment(time).format("HH:mm") : null;
  },

  formatTime_YYYYMMDD(time) {
    return time != null ? moment(time).format("YYYY-MM-DD") : null;
  },

  currentTime_YYYYMMDDHHMMSS() {
    return moment(new Date()).format("YYYY-MM-DDTHH:mm:ss");
  },

  round(value) {
    return Math.round(value * 100) / 100;
  },
};

export default Helper;
