import moment from "moment";
import SpendingStatistics from "../Model/SpendingStatistics";
import Constants from "./Constants";

const createStatistics = (budgetDto, spendingOnly, includePending = false) => {
  const start = moment([budgetDto.year, budgetDto.month - 1, 1]);
  const end = start.clone().add(1, "month");

  const days = getDays(start, end);
  const passedDays = getPassedDays(start, end);
  const remainingDays = getRemainingDays(start, end);

  const expectedDailySpending = budgetDto.volume / days;
  const expectedTotalSpending = expectedDailySpending * passedDays;

  const totalSpending = getTotalSpending(
    budgetDto.transactions,
    spendingOnly,
    includePending
  );
  const dailySpending = passedDays > 0 ? totalSpending / passedDays : 0;

  const totalDeviation = expectedTotalSpending - totalSpending;
  const dailyDeviation = expectedDailySpending - dailySpending;

  const totalRemainingVolume = Math.max(budgetDto.volume - totalSpending, 0);
  const dailyRemainingVolume =
    remainingDays > 0 ? Math.max(totalRemainingVolume / remainingDays, 0) : 0;

  const todaysSpending = getTodaysSpending(
    budgetDto.transactions,
    spendingOnly,
    includePending
  );

  const transactionCount = getTransactionCount(
    budgetDto.transactions,
    spendingOnly,
    includePending
  );

  const todaysTransactionCount = getTodaysTransactionCount(
    budgetDto.transactions,
    spendingOnly,
    includePending
  );

  return new SpendingStatistics({
    totalSpending,
    dailySpending,
    totalDeviation,
    dailyDeviation,
    totalRemainingVolume,
    dailyRemainingVolume,
    todaysSpending,
    transactionCount,
    todaysTransactionCount,
  });
};

const getDays = (start, end) => end.diff(start, "days");

const getPassedDays = (start, end) => {
  const now = moment();
  if (now.isBefore(start)) {
    return 0;
  } else if (now.isBefore(end)) {
    return now.diff(start, "days") + 1;
  } else {
    return getDays(start, end);
  }
};

const getRemainingDays = (start, end) =>
  getDays(start, end) - getPassedDays(start, end);

const getTotalSpending = (transactions, spendingOnly, includePending) =>
  getFilteredTransactions(transactions, spendingOnly, includePending)
    .map((transaction) => transaction.amount)
    .reduce((total, transactionAmount) => total + transactionAmount, 0);

const getTodaysSpending = (transactions, spendingOnly, includePending) =>
  getTodaysTransactions(transactions, spendingOnly, includePending)
    .map((transaction) => transaction.amount)
    .reduce((total, transactionAmount) => total + transactionAmount, 0);

const getTransactionCount = (transactions, spendingOnly, includePending) =>
  getFilteredTransactions(transactions, spendingOnly, includePending).length;

const getTodaysTransactionCount = (
  transactions,
  spendingOnly,
  includePending
) => getTodaysTransactions(transactions, spendingOnly, includePending).length;

const getFilteredTransactions = (transactions, spendingOnly, includePending) =>
  transactions
    .filter(
      (transaction) =>
        includePending ||
        transaction.status === Constants.TRANSACTION_STATUS.APPROVED
    )
    .filter(
      (transaction) =>
        !spendingOnly || (transaction.amount > 0 && !transaction.internal)
    );

const getTodaysTransactions = (transactions, spendingOnly, includePending) => {
  const todayMidnight = moment().startOf("day");
  const tomorrowMidnight = todayMidnight.clone().add(1, "days");
  let filteredTransactions = getFilteredTransactions(
    transactions,
    spendingOnly,
    includePending
  );
  filteredTransactions = filteredTransactions.filter((transaction) =>
    moment(transaction.time).isBetween(
      todayMidnight,
      tomorrowMidnight,
      null,
      "[)"
    )
  );
  return filteredTransactions;
};

const StatisticsFactory = {
  createStatistics,
  getTotalSpending,
};

export default StatisticsFactory;