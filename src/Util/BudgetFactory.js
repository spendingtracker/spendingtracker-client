import Budget from "../Model/Budget";

const createBudgets = budgetDtos =>
  budgetDtos.map(budgetDto => createBudget(budgetDto));

const createBudget = budgetDto => {
  return new Budget({ ...budgetDto });
};

const BudgetFactory = {
  createBudgets
};

export default BudgetFactory;