const Constants = {
  TRANSACTION_STATUS: {
    APPROVED: "APPROVED",
    PENDING: "PENDING",
  },
};

export default Constants;