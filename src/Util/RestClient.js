import Helper from "./Helper";
import moment from "moment";

const RestClient = {
  getCategories() {
    let url = process.env.REACT_APP_API_URL;
    url = `${url}/categories`;
    return this.get(url).then((response) => response.json());
  },

  postCategory(category) {
    let url = `${process.env.REACT_APP_API_URL}/categories/category`;
    return this.post(url, category);
  },

  putCategory(category) {
    let url = `${process.env.REACT_APP_API_URL}/categories/category`;
    return this.put(url, category);
  },

  deleteCategory(id) {
    let url = `${process.env.REACT_APP_API_URL}/categories/category/${id}`;
    return this.delete(url);
  },

  restoreCategory(id) {
    let url = `${process.env.REACT_APP_API_URL}/categories/category/${id}/restore`;
    return fetch(url, {
      method: "PUT",
      headers: this.addAuthorizationHeader({}),
    });
  },

  getBudgets(monthOffset) {
    const period = Helper.getOffsetPeriod(monthOffset);
    let url = process.env.REACT_APP_API_URL;
    url = `${url}/budgets/${period.year}/${period.month}`;
    return this.get(url).then((response) => response.json());
  },

  getBudgetsForPeriod(periodFrom, periodTo) {
    let url = process.env.REACT_APP_API_URL;
    url = `${url}/budgets/from/${periodFrom.year}/${periodFrom.month}/to/${periodTo.year}/${periodTo.month}`;
    return this.get(url).then((response) => response.json());
  },

  postBudget(budget) {
    return this.post(`${process.env.REACT_APP_API_URL}/budgets/budget`, budget);
  },

  postBudgets(budgets) {
    return this.post(`${process.env.REACT_APP_API_URL}/budgets`, budgets);
  },

  putBudget(budget) {
    return this.put(`${process.env.REACT_APP_API_URL}/budgets/budget`, budget);
  },

  postTransaction(transaction) {
    return this.post(
      `${process.env.REACT_APP_API_URL}/transactions/transaction`,
      transaction
    );
  },

  postTransactions(transactions) {
    return this.post(
      `${process.env.REACT_APP_API_URL}/transactions`,
      transactions
    );
  },

  putTransaction(transaction) {
    return this.put(
      `${process.env.REACT_APP_API_URL}/transactions/transaction`,
      transaction
    );
  },

  deleteTransaction(id) {
    return this.delete(
      `${process.env.REACT_APP_API_URL}/transactions/transaction/${id}`
    );
  },

  login(credentials) {
    return this.post(
      `${process.env.REACT_APP_API_URL}/login`,
      credentials
    ).then(this.setToken);
  },

  get(url) {
    return fetch(this.nonPayloadRequest(url, "GET")).then((response) =>
      this.validate(response)
    );
  },

  delete(url) {
    return fetch(this.nonPayloadRequest(url, "DELETE")).then((response) =>
      this.validate(response)
    );
  },

  put(url, body) {
    return fetch(this.payloadRequest(url, "PUT", body)).then((response) =>
      this.validate(response)
    );
  },

  post(url, body) {
    return fetch(this.payloadRequest(url, "POST", body)).then((response) =>
      this.validate(response)
    );
  },

  nonPayloadRequest(url, method) {
    return new Request(url, {
      method: method,
      headers: this.addAuthorizationHeader({}),
    });
  },

  payloadRequest(url, method, body) {
    return new Request(url, {
      method: method,
      headers: this.addAuthorizationHeader({
        "Content-Type": "application/json",
      }),
      body: JSON.stringify(body),
    });
  },

  validate(response) {
    if (response.status < 200 || response.status >= 300) {
      this.clearToken();
    }
    return response;
  },

  setToken(response) {
    if (response != null && response.headers.get("Authorization") != null) {
      localStorage.setItem("token", response.headers.get("Authorization"));
    }
    if (response != null && response.headers.get("Expires") != null) {
      localStorage.setItem("expires", response.headers.get("Expires"));
    }
  },

  clearToken() {
    localStorage.removeItem("token");
    localStorage.removeItem("expires");
  },

  getToken() {
    return localStorage.getItem("token");
  },

  getExpires() {
    return localStorage.getItem("expires");
  },

  getExpiresInMinutes() {
    let expiryPeriod = Math.floor(
      moment.duration(moment(this.getExpires()).diff(moment())).asMinutes()
    );
    return Math.max(expiryPeriod, 0);
  },

  addAuthorizationHeader(headers) {
    headers["Authorization"] = this.getToken();
    return headers;
  },

  isAuthenticated() {
    return this.getToken() != null;
  },
};

export default RestClient;
