import React from "react";

const ActualSpending = ({ budget, statistics }) => {
  const totalDeviation = statistics.totalDeviation;
  const totalSpending = statistics.totalSpending;
  const todaysSpending = statistics.todaysSpending;
  const dailySpending = statistics.dailySpending;
  const dailyVolume = budget.dailyVolume;
  const dailyDeviation = statistics.dailyDeviation;
  const totalVolume = budget.volume;

  if (budget.trackingPeriod === "DAILY") {
    return (
      <div>
        <div className="top-note">ACTUAL (TODAY)</div>
        <div
          className={
            totalDeviation >= 0 ? "value-large-green" : "value-large-red"
          }
        >
          €{totalSpending}{" "}
          <span
            className={
              todaysSpending <= dailyVolume
                ? "value-large-green"
                : "value-large-red"
            }
          >
            (€{todaysSpending})
          </span>
        </div>
        <div
          className={
            dailyDeviation >= 0 ? "value-small-green" : "value-small-red"
          }
        >
          €{dailySpending} / DAY
        </div>
      </div>
    );
  } else {
    return (
      <div>
        <div className="top-note">ACTUAL (TODAY)</div>
        <div
          className={
            totalSpending <= totalVolume
              ? "value-large-green"
              : "value-large-red"
          }
        >
          €{totalSpending} (€{todaysSpending})
        </div>
      </div>
    );
  }
};

export default ActualSpending;