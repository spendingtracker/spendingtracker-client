import React from "react";

const Recommendation = ({ budget, statistics }) => {
  const totalDeviation = statistics.totalDeviation;
  const dailyRemaining = statistics.dailyRemainingVolume;
  const totalRemaining = statistics.totalRemainingVolume;
  const totalSpending = statistics.totalSpending;
  const totalVolume = budget.volume;

  let recommendation = "";
  if (budget.trackingPeriod === "MONTHLY") {
    if (totalSpending <= totalVolume) {
      recommendation = `You have €${totalRemaining} left.`;
    } else {
      recommendation = "You are broke.";
    }
  } else {
    if (totalDeviation >= 0) {
      recommendation = `You can afford spending €${dailyRemaining} per day.`;
    } else {
      if (totalRemaining > 0) {
        recommendation = `Spend €${dailyRemaining} per day and you are on budget.`;
      } else {
        recommendation = "You are broke.";
      }
    }
  }

  return (
    <table className="light-box">
      <tbody>
        <tr>
          <td className="text-center">{recommendation}</td>
        </tr>
      </tbody>
    </table>
  );
};

export default Recommendation;
