import React from "react";
import { Input } from "reactstrap";

const TrackingPeriodSelector = ({ value, onChange }) => {
  let trackingPeriods = [
    { title: "Monthly tracking", value: "MONTHLY" },
    { title: "Daily tracking", value: "DAILY" }
  ];

  return (
    <div className="form-row-content">
      <div className="top-note">Tracking type</div>
      <Input
        type="select"
        onChange={onChange}
        style={{ textAlignLast: "center" }}
        value={value}
      >
        {trackingPeriods.map(period => (
          <option key={period.value} value={period.value}>
            {period.title}
          </option>
        ))}
      </Input>
    </div>
  );
};

export default TrackingPeriodSelector;