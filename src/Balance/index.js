import React, { Component } from "react";
import { connect } from "react-redux";
import Selectors from "../redux/selectors";
import View from "./View";
import Edit from "./Edit";
import Transfer from "./Transfer";

const MODE_VIEW = "MODE_VIEW";
const MODE_EDIT = "MODE_EDIT";
const MODE_TRANSFER = "MODE_TRANSFER";

class Statistics extends Component {
  state = { mode: MODE_VIEW };

  handleEditOn = () => {
    this.setState({ mode: MODE_EDIT });
  };

  handleTransferOn = () => {
    this.setState({ mode: MODE_TRANSFER });
  };

  handleViewOn = () => {
    this.setState({ mode: MODE_VIEW });
  };

  render() {
    if (!this.props.budget.id || this.state.mode === MODE_EDIT) {
      return <Edit onClose={this.handleViewOn} />;
    } else if (this.state.mode === MODE_TRANSFER) {
      return <Transfer onClick={this.handleViewOn} />;
    } else {
      return (
        <View
          onEditButtonClick={this.handleEditOn}
          onTransferButtonClick={this.handleTransferOn}
        />
      );
    }
  }
}

const mapStateToProps = (state) => ({
  budget: Selectors.getSelectedBudget(state),
});

export default connect(mapStateToProps, null)(Statistics);
