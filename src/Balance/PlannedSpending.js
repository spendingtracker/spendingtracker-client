import React from "react";

const PlannedSpending = ({ budget }) => {
  const subtitle = budget.trackingPeriod === "DAILY" && (
    <div className="value-small-grey">€{budget.dailyVolume} / DAY</div>
  );
  return (
    <div>
      <div className="top-note">PLANNED</div>
      <div className="value-large-grey">€{budget.volume}</div>
      {subtitle}
    </div>
  );
};

export default PlannedSpending;