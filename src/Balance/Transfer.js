import React, { Component } from "react";
import { connect } from "react-redux";
import Selectors from "../redux/selectors";
import Actions from "../redux/actions";
import { Form, ButtonGroup, Button } from "reactstrap";
import MoneyInput from "../InputControls/MoneyInput";
import TargetBudgetSelector from "./TargetBudgetSelector";
import { IoMdAddCircle, IoMdRemoveCircle, IoMdClose } from "react-icons/io";
import moment from "moment";
import StatisticsFactory from "../Util/StatisticsFactory";
import Helper from "../Util/Helper";
import Constants from "../Util/Constants";

class Transfer extends Component {
  getInitialAmountValue = () => {
    const statistics = StatisticsFactory.createStatistics(
      this.props.selectedBudget,
      false,
      true
    );
    return Helper.round(
      Math.abs(this.props.selectedBudget.volume - statistics.totalSpending)
    );
  };

  state = {
    amountValue: this.getInitialAmountValue(),
    targetBudget: {}
  };

  componentDidUpdate(prevProps) {
    if (prevProps.selectedBudget.id !== this.props.selectedBudget.id) {
      this.setState({
        amountValue: this.getInitialAmountValue(),
        targetBudget: {}
      });
    }
  }

  handleAmountChange = amount => this.setState({ amountValue: amount });

  handleTargetBudgetChange = targetBudget =>
    this.setState({ targetBudget: targetBudget });

  generateTransaction = (amount, budgetId, comment) => {
    return {
      time: moment(new Date()).format("YYYY-MM-DDTHH:mm:ss"),
      amount: amount,
      comment: comment,
      internal: true,
      status: Constants.TRANSACTION_STATUS.APPROVED,
      budgetId: budgetId
    };
  };

  handleSave = event => {
    let amount = this.state.amountValue;
    if (amount > 0 && this.state.targetBudget.id > 0) {
      let transactions = [];
      if (event.target.id === "pullButton") {
        transactions.push(
          this.generateTransaction(
            amount * -1,
            this.props.selectedBudget.id,
            `Pull of funds from '${this.state.targetBudget.category.name}'`
          )
        );
        transactions.push(
          this.generateTransaction(
            amount,
            this.state.targetBudget.id,
            `Push of funds to '${this.props.selectedBudget.category.name}'`
          )
        );
      } else {
        transactions.push(
          this.generateTransaction(
            amount,
            this.props.selectedBudget.id,
            `Push of funds to '${this.state.targetBudget.category.name}'`
          )
        );
        transactions.push(
          this.generateTransaction(
            amount * -1,
            this.state.targetBudget.id,
            `Pull of funds from '${this.props.selectedBudget.category.name}'`
          )
        );
      }
      this.props.postTransactions(transactions);
      this.setState({ amountValue: 0, targetBudget: {} });
      this.props.onClick();
    }
  };

  renderPushButton = () => (
    <Button id="pushButton" onClick={this.handleSave}>
      <IoMdRemoveCircle /> Push
    </Button>
  );

  renderCancelButton = () => (
    <Button onClick={() => this.props.onClick()}>
      <IoMdClose /> Cancel
    </Button>
  );

  renderPullButton = () => (
    <Button id="pullButton" onClick={this.handleSave}>
      <IoMdAddCircle /> Pull
    </Button>
  );

  renderTransferForm = () => (
    <div>
      <MoneyInput
        id="amount"
        title="Amount"
        amount={this.state.amountValue}
        onValueChange={this.handleAmountChange}
      />
      <br />
      <TargetBudgetSelector onChange={this.handleTargetBudgetChange} />
    </div>
  );

  renderNoBudgetsMessage = () => (
    <div className="text-center general-note">
      No target budgets! Transfers not possible.
    </div>
  );

  render() {
    return (
      <Form className="form-row-content">
        <div className="text-center general-note">
          Transfer funds between budgets
        </div>
        {this.props.targetBudgets.length > 0 && this.renderTransferForm()}
        <br />
        <div className="text-center">
          {this.props.targetBudgets.length < 1 && this.renderNoBudgetsMessage()}
          <br />
          <ButtonGroup>
            {this.props.targetBudgets.length > 0 && this.renderPushButton()}
            {this.renderCancelButton()}
            {this.props.targetBudgets.length > 0 && this.renderPullButton()}
          </ButtonGroup>
        </div>
      </Form>
    );
  }
}

const mapStateToProps = state => ({
  selectedBudget: Selectors.getSelectedBudget(state),
  targetBudgets: Selectors.getBudgets(state).filter(
    budget => budget.id !== Selectors.getSelectedBudget(state).id
  )
});

const mapDispatchToProps = dispatch => ({
  postTransactions: transactions =>
    dispatch(Actions.postTransactions(transactions))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Transfer);
