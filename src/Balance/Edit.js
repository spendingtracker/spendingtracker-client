import React, { Component } from "react";
import { IoMdClose, IoMdSave } from "react-icons/io";
import { connect } from "react-redux";
import { Button, ButtonGroup } from "reactstrap";
import MoneyInput from "../InputControls/MoneyInput";
import Actions from "../redux/actions";
import Selectors from "../redux/selectors";
import Helper from "../Util/Helper";
import TrackingPeriodSelector from "./TrackingPeriodSelector";

class Edit extends Component {
  state = {
    dailyVolume: this.props.budget.dailyVolume || "",
    monthlyVolume: this.props.budget.volume || "",
    trackingPeriod: this.props.budget.trackingPeriod || "MONTHLY",
  };

  componentDidUpdate(prevProps) {
    if (
      this.props.budget.dailyVolume !== prevProps.budget.dailyVolume ||
      this.props.budget.monthlyVolume !== prevProps.budget.monthlyVolume ||
      this.props.budget.trackingPeriod !== prevProps.budget.trackingPeriod
    ) {
      this.setState({
        dailyVolume: this.props.budget.dailyVolume || "",
        monthlyVolume: this.props.budget.volume || "",
        trackingPeriod: this.props.budget.trackingPeriod || "MONTHLY",
      });
    }
  }

  handleVolumeChange = (amount) => {
    if (this.state.trackingPeriod === "DAILY") {
      this.setState({ dailyVolume: amount });
    } else {
      this.setState({ monthlyVolume: amount });
    }
  };

  handleTrackingPeriodChange = (event) => {
    this.setState({ trackingPeriod: event.target.value });
  };

  handleSave = () => {
    const dailyVolume =
      this.state.dailyVolume > 0 ? this.state.dailyVolume : 0.0;
    const monthlyVolume =
      this.state.monthlyVolume > 0 ? this.state.monthlyVolume : 0.0;

    const period = Helper.getOffsetPeriod(this.props.monthOffset);
    const volume = Helper.round(
      this.state.trackingPeriod === "DAILY"
        ? dailyVolume * period.days
        : monthlyVolume
    );

    let budget = {
      ...this.props.budget,
      category: this.props.category,
      volume: volume,
      year: period.year,
      month: period.month,
      trackingPeriod: this.state.trackingPeriod,
    };

    this.props.saveBudget(budget);
    this.props.onClose();
  };

  renderCancelButton() {
    return (
      <Button onClick={this.props.onClose}>
        <IoMdClose /> Cancel
      </Button>
    );
  }

  render() {
    return (
      <div className="form-row-content">
        <div className="value-small-grey text-center form-row-content">
          DEFINE BUDGET
        </div>
        <TrackingPeriodSelector
          value={this.state.trackingPeriod}
          onChange={this.handleTrackingPeriodChange}
        />
        <MoneyInput
          id="dailyVolume"
          title={
            this.state.trackingPeriod === "DAILY"
              ? "Daily volume"
              : "Monthly volume"
          }
          amount={
            this.state.trackingPeriod === "DAILY"
              ? this.state.dailyVolume
              : this.state.monthlyVolume
          }
          onValueChange={this.handleVolumeChange}
        />
        <br />
        <div className="text-center">
          <ButtonGroup>
            {this.props.budget.id > 0 && this.renderCancelButton()}
            <Button onClick={this.handleSave}>
              <IoMdSave /> Save
            </Button>
          </ButtonGroup>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  monthOffset: Selectors.getMonthOffset(state),
  category: Selectors.getSelectedCategory(state),
  budget: Selectors.getSelectedBudget(state),
});

const mapDispatchToProps = (dispatch) => ({
  saveBudget: (budget) => dispatch(Actions.saveBudget(budget)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Edit);
