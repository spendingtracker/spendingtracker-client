import React, { Component } from "react";
import { IoIosCreate, IoIosSend } from "react-icons/io";
import { connect } from "react-redux";
import { Button, ButtonGroup, Col, Row } from "reactstrap";
import GreyCheckbox from "../InputControls/GreyCheckbox";
import Selectors from "../redux/selectors";
import ActualSpending from "./ActualSpending";
import Deviation from "./Deviation";
import PlannedSpending from "./PlannedSpending";
import Recommendation from "./Recommendation";
import TxCount from "./TxCount";
import StatisticsFactory from "../Util/StatisticsFactory";

class View extends Component {
  state = { spendingOnly: false, includePending: false };

  toggleSpendingOnly = spendingOnly =>
    this.setState({ spendingOnly: spendingOnly });

  toggleIncludePending = includePending =>
    this.setState({ includePending: includePending });

  getStatistics = () =>
    StatisticsFactory.createStatistics(
      this.props.budget,
      this.state.spendingOnly,
      this.state.includePending
    );

  render() {
    const budget = this.props.budget;
    const statistics = this.getStatistics();

    return (
      <div className="form-row-content">
        <Row className="text-center">
          <Col className="col-6">
            <PlannedSpending budget={budget} />
          </Col>
          <Col className="col-6">
            <ActualSpending budget={budget} statistics={statistics} />
          </Col>
        </Row>
        <Row className="text-center">
          <Col className="col-6">
            <TxCount
              count={statistics.transactionCount}
              todaysCount={statistics.todaysTransactionCount}
            />
          </Col>
          <Col className="col-6">
            <Deviation budget={budget} statistics={statistics} />
          </Col>
        </Row>
        <Row className="text-center">
          <Col>
            <Recommendation budget={budget} statistics={statistics} />
          </Col>
        </Row>
        <Row className="text-center">
          <Col>
            <GreyCheckbox
              checked={this.state.spendingOnly}
              onChange={this.toggleSpendingOnly}
              text="Spending only"
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <GreyCheckbox
              checked={this.state.includePending}
              onChange={this.toggleIncludePending}
              text="Include pending"
            />
          </Col>
        </Row>
        <Row className="text-center">
          <Col>
            <br />
            <ButtonGroup>
              <Button onClick={this.props.onEditButtonClick}>
                <IoIosCreate /> Modify
              </Button>
              <Button onClick={this.props.onTransferButtonClick}>
                <IoIosSend /> Transfer
              </Button>
            </ButtonGroup>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  budget: Selectors.getSelectedBudget(state)
});

export default connect(
  mapStateToProps,
  null
)(View);
