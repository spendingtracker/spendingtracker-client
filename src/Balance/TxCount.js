import React from "react";

const TxCount = ({ count, todaysCount }) => {
  return (
    <div>
      <div className="top-note">COUNT (TODAY)</div>
      <div className="value-large-black">
        {count} ({todaysCount})
      </div>
      <div className="value-small-black">
        {count === 1 ? "TRANSACTION" : "TRANSACTIONS"}
      </div>
    </div>
  );
};

export default TxCount;
