import React, { Component } from "react";
import { connect } from "react-redux";
import Selectors from "../redux/selectors";
import { Input } from "reactstrap";

class TargetBudgetSelector extends Component {
  targetBudgetChanged = (event) => {
    const budgetId = parseInt(event.target.value);
    this.props.onChange(
      this.props.targetBudgets.find((budget) => budget.id === budgetId)
    );
  };

  render() {
    return (
      <div className="form-row-content">
        <div className="top-note">Target budget</div>
        <Input
          type="select"
          onChange={this.targetBudgetChanged}
          style={{ textAlignLast: "center" }}
        >
          <option value="" />
          {this.props.targetBudgets.map((budget) => (
            <option key={budget.id} value={budget.id}>
              {budget.category.name}
            </option>
          ))}
        </Input>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  targetBudgets: Selectors.getBudgets(state).filter(
    (budget) => budget.id !== Selectors.getSelectedBudget(state).id
  ),
});

export default connect(mapStateToProps, null)(TargetBudgetSelector);
