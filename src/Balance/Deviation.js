import React from "react";
import Helper from "../Util/Helper";

const Deviation = ({ budget, statistics }) => {
  if (budget.trackingPeriod === "DAILY") {
    const totalDeviation = statistics.totalDeviation;
    const dailyDeviation = statistics.dailyDeviation;
    const deviationSign = totalDeviation >= 0 ? "+" : "-";

    return (
      <div>
        <div className="top-note">DEVIATION</div>
        <div
          className={
            totalDeviation >= 0 ? "value-large-green" : "value-large-red"
          }
        >
          €{deviationSign}
          {Math.abs(totalDeviation)}
        </div>
        <div
          className={
            totalDeviation >= 0 ? "value-small-green" : "value-small-red"
          }
        >
          €{deviationSign}
          {Math.abs(dailyDeviation)} / DAY
        </div>
      </div>
    );
  } else {
    const totalSpending = statistics.totalSpending;
    const plannedSpending = budget.volume;
    const spendingPercentage = Helper.round(
      (totalSpending / plannedSpending) * 100
    );

    return (
      <div>
        <div className="top-note">Spending percentage</div>
        <div
          className={
            spendingPercentage <= 100 || plannedSpending === 0
              ? "value-large-green"
              : "value-large-red"
          }
        >
          {plannedSpending > 0 ? spendingPercentage + "%" : "N/A"}
        </div>
        <div className="value-small-black">FROM TOTAL</div>
      </div>
    );
  }
};

export default Deviation;
