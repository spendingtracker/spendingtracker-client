import React from "react";
import Helper from "../Util/Helper";
import StatisticsFactory from "../Util/StatisticsFactory";

const Footer = ({ budgets, spendingOnly, includePending }) => {
  const allPlannedSpending = Helper.round(
    budgets.reduce((total, budget) => total + budget.volume, 0)
  );
  const allActualSpending = Helper.round(
    budgets.reduce((total, budget) => {
      const statistics = StatisticsFactory.createStatistics(
        budget,
        spendingOnly,
        includePending
      );
      return total + statistics.totalSpending;
    }, 0)
  );
  const allBalance = Helper.round(allPlannedSpending - allActualSpending);

  return (
    <React.Fragment>
      <tr>
        <td colSpan="3">
          <div className="caption-2-strong border-top text-align-left">
            <b>Total of all budgets</b>
          </div>
        </td>
      </tr>
      <tr>
        <td width="34%">
          <div className="value-small-grey-strong text-align-left">
            {allPlannedSpending} €
          </div>
        </td>
        <td width="33%">
          <div className="value-small-grey-strong text-align-left">
            {allActualSpending} €
          </div>
        </td>
        <td width="33%">
          <div className="text-align-left">
            <span
              className={
                allBalance >= 0
                  ? "value-small-green-strong"
                  : "value-small-red-strong"
              }
            >
              {allBalance} €
            </span>
          </div>
        </td>
      </tr>
    </React.Fragment>
  );
};

export default Footer;