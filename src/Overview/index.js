import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from "@material-ui/core";
import React, { Component } from "react";
import { connect } from "react-redux";
import Selectors from "../redux/selectors";
import Footer from "./Footer";
import Header from "./Header";
import Statistics from "./Statistics";
import BudgetSetCloner from "./BudgetSetCloner";
import PeriodStatistics from "./PeriodStatistics";

class Overview extends Component {
  state = {
    spendingOnly: false,
    includePending: false,
    sortByPlannedSpendingAsc: null,
    sortByTotalSpendingAsc: null,
    sortByTotalDeviationAsc: null,
  };

  toggleSpendingOnly = (checked) => this.setState({ spendingOnly: checked });

  toggleIncludePending = (includePending) =>
    this.setState({ includePending: includePending });

  toggleSortByPlannedSpendingAsc = () =>
    this.setState({
      sortByPlannedSpendingAsc: !this.state.sortByPlannedSpendingAsc,
      sortByTotalSpendingAsc: null,
      sortByTotalDeviationAsc: null,
    });

  toggleSortByTotalSpendingAsc = () =>
    this.setState({
      sortByPlannedSpendingAsc: null,
      sortByTotalSpendingAsc: !this.state.sortByTotalSpendingAsc,
      sortByTotalDeviationAsc: null,
    });

  toggleSortByTotalDeviationAsc = () => {
    return this.setState({
      sortByPlannedSpendingAsc: null,
      sortByTotalSpendingAsc: null,
      sortByTotalDeviationAsc: !this.state.sortByTotalDeviationAsc,
    });
  };

  render() {
    if (this.props.budgets.length > 0) {
      return (
        <React.Fragment>
          <Accordion>
            <AccordionSummary>
              <div className="caption-1">Statistics for month</div>
            </AccordionSummary>
            <AccordionDetails>
              <table width="100%">
                <Header
                  spendingOnly={this.state.spendingOnly}
                  toggleSpendingOnly={this.toggleSpendingOnly}
                  includePending={this.state.includePending}
                  toggleIncludePending={this.toggleIncludePending}
                  toggleSortByPlannedSpending={
                    this.toggleSortByPlannedSpendingAsc
                  }
                  toggleSortByTotalSpending={this.toggleSortByTotalSpendingAsc}
                  toggleSortByTotalDeviation={
                    this.toggleSortByTotalDeviationAsc
                  }
                />
                <tbody>
                  <Statistics
                    spendingOnly={this.state.spendingOnly}
                    includePending={this.state.includePending}
                    budgets={this.props.budgets}
                    sortByPlannedSpendingAsc={
                      this.state.sortByPlannedSpendingAsc
                    }
                    sortByTotalSpendingAsc={this.state.sortByTotalSpendingAsc}
                    sortByTotalDeviationAsc={this.state.sortByTotalDeviationAsc}
                  />
                  <Footer
                    budgets={this.props.budgets}
                    spendingOnly={this.state.spendingOnly}
                    includePending={this.state.includePending}
                  />
                </tbody>
              </table>
            </AccordionDetails>
          </Accordion>
          <Accordion>
            <AccordionSummary>
              <div className="caption-1">Statistics for period</div>
            </AccordionSummary>
            <AccordionDetails>
              <PeriodStatistics />
            </AccordionDetails>
          </Accordion>
        </React.Fragment>
      );
    } else {
      return <BudgetSetCloner />;
    }
  }
}

const mapStateToProps = (state) => ({
  budgets: Selectors.getBudgets(state),
});

export default connect(mapStateToProps, null)(Overview);
