import React, { Component } from "react";
import { connect } from "react-redux";
import Selectors from "../../redux/selectors";
import Helper from "../../Util/Helper";
import RestClient from "../../Util/RestClient";
import ActualAverageSpendingByBudget from "./results/ActualAverageSpendingByBudget";
import ActualAverageSpendingByMonth from "./results/ActualAverageSpendingByMonth";
import ActualTotalSpending from "./results/ActualTotalSpending";
import BudgetCount from "./results/BudgetCount";
import MonthCount from "./results/MonthCount";
import PlannedAverageSpendingByBudget from "./results/PlannedAverageSpendingByBudget";
import PlannedAverageSpendingByMonth from "./results/PlannedAverageSpendingByMonth";
import PlannedTotalSpending from "./results/PlannedTotalSpending";
import BudgetSelector from "./selectors/BudgetSelector";
import MonthSelector from "./selectors/MonthSelector";

class PeriodStatistics extends Component {
  state = {
    budgets: [],
    selectedBudgets: [],
    selectedCategoryId: 0,
    periodFrom: null,
    periodTo: null,
  };

  periodFromChanged = event => {
    this.setState(
      {
        selectedBudgets: [],
        selectedCategoryId: 0,
        periodFrom: parseInt(event.target.value) !== 0 ? Helper.getOffsetPeriod(event.target.value) : null,
      },
      this.retrieveBudgets
    );
  };

  periodToChanged = event => {
    this.setState(
      {
        selectedBudgets: [],
        selectedCategoryId: 0,
        periodTo: parseInt(event.target.value) !== 0 ? Helper.getOffsetPeriod(event.target.value) : null,
      },
      this.retrieveBudgets
    );
  };

  retrieveBudgets = async () => {
    if (this.state.periodFrom == null || this.state.periodTo == null) {
      this.setState({
        budgets: [],
        selectedBudgets: [],
        selectedCategoryId: 0,
      });
    } else {
      const budgets = await RestClient.getBudgetsForPeriod(
        this.state.periodFrom,
        this.state.periodTo
      );
      this.setState({ budgets: budgets });
    }
  };

  handleCategoryChange = event => {
    if (
      parseInt(event.target.value) === 0 ||
      this.state.periodFrom == null ||
      this.state.periodTo == null
    ) {
      this.setState({ selectedCategoryId: 0, selectedBudgets: [] });
    } else {
      const selectedBudgets = this.state.budgets.filter(b => b.category.id === parseInt(event.target.value));
      this.setState({
        selectedCategoryId: parseInt(event.target.value),
        selectedBudgets: selectedBudgets,
      });
    }
  };

  render() {
    return (
      <div className="menu-content">
        {this.renderSelectors()}
        <br />
        {this.renderResults()}
      </div>
    );
  }

  renderSelectors() {
    return (
      <div className="menu-content-item">
        <MonthSelector note="FROM" onMonthChanged={this.periodFromChanged} />
        <MonthSelector note="TO" onMonthChanged={this.periodToChanged} />
        <BudgetSelector categories={this.props.categories} selectedCategoryId={this.state.selectedCategoryId} onCategoryChanged={this.handleCategoryChange} />
      </div>
    );
  }

  renderResults() {
    return (
      <div className="row">
        <MonthCount periodTo={this.state.periodTo} periodFrom={this.state.periodFrom} />
        <BudgetCount selectedBudgets={this.state.selectedBudgets} />
        <div className="text-center col-12">
          <hr />
          <div className="caption-1">TOTAL SPENDING</div>
        </div>
        <PlannedTotalSpending selectedBudgets={this.state.selectedBudgets} />
        <ActualTotalSpending selectedBudgets={this.state.selectedBudgets} />
        <div className="text-center col-12">
          <hr />
          <div className="caption-1">AVERAGE SPENDING BY MONTH</div>
        </div>
        <PlannedAverageSpendingByMonth selectedBudgets={this.state.selectedBudgets} periodTo={this.state.periodTo} periodFrom={this.state.periodFrom} />
        <ActualAverageSpendingByMonth selectedBudgets={this.state.selectedBudgets} periodTo={this.state.periodTo} periodFrom={this.state.periodFrom} />
        <div className="text-center col-12">
          <hr />
          <div className="caption-1">AVERAGE SPENDING BY BUDGET</div>
        </div>
        <PlannedAverageSpendingByBudget selectedBudgets={this.state.selectedBudgets} />
        <ActualAverageSpendingByBudget selectedBudgets={this.state.selectedBudgets} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  categories: Selectors.getCategories(state),
});

export default connect(mapStateToProps, null)(PeriodStatistics);
