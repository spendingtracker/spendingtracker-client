import React from "react";
import { Input } from "reactstrap";
import Util from "../Util";

const MonthSelector = ({ note, onMonthChanged }) => (
  <React.Fragment>
    <div className="text-align-left">
      <div className="top-note">{note}</div>
    </div>
    <Input
      type="select"
      onChange={onMonthChanged}
      style={{ textAlignLast: "center" }}
      className="form-control"
    >
      <option key={0} value={0}></option>
      {Util.getPastPeriods().map((p) => {
        return (
          <option key={p.text} value={p.monthOffset}>
            {p.text}
          </option>
        );
      })}
    </Input>
  </React.Fragment>
);

export default MonthSelector;