import React from "react";
import { Input } from "reactstrap";
import Util from "../Util";

const BudgetSelector = ({ categories, selectedCategoryId, onCategoryChanged }) => (
  <React.Fragment>
    <div className="text-align-left">
      <div className="top-note">BUDGET</div>
    </div>
    <Input
      type="select"
      onChange={onCategoryChanged}
      style={{ textAlignLast: "center" }}
      className="form-control"
      value={selectedCategoryId}
    >
      <option key={0} value={0}></option>
      {Util.getSortedCategories(categories).map((c) => {
        return (
          <option key={c.id} value={c.id}>
            {c.deleted && "[DELETED] "} {c.name}
          </option>
        );
      })}
    </Input>
  </React.Fragment>
);

export default BudgetSelector;