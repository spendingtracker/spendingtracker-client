import React from "react";
import Util from "../Util";

const ActualAverageSpendingByMonth = ({ selectedBudgets, periodTo, periodFrom }) => (
  <div className="text-center col-6">
    <div className="top-note">ACTUAL</div>
    <div
      className={
        Util.calculateActualAverageSpendingByMonth(selectedBudgets, periodTo, periodFrom) >
          Util.calculatePlannedAverageSpendingByMonth(selectedBudgets, periodTo, periodFrom)
          ? "value-large-red"
          : "value-large-green"
      }
    >
      {Util.calculateActualAverageSpendingByMonth(selectedBudgets, periodTo, periodFrom)} €
    </div>
  </div>
);

export default ActualAverageSpendingByMonth;