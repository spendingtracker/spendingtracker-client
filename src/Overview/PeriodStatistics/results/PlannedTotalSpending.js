import React from "react";
import Util from "../Util";

const PlannedTotalSpending = ({ selectedBudgets }) => (
  <div className="text-center col-6">
    <div className="top-note">PLANNED</div>
    <div className="value-large-grey">
      {Util.calculatePlannedTotalSpending(selectedBudgets)} €
    </div>
  </div>
);

export default PlannedTotalSpending;