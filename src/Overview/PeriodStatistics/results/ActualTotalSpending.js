import React from "react";
import Util from "../Util";

const ActualTotalSpending = ({ selectedBudgets }) => (
  <div className="text-center col-6">
    <div className="top-note">ACTUAL</div>
    <div
      className={
        Util.calculateActualTotalSpending(selectedBudgets) > Util.calculatePlannedTotalSpending(selectedBudgets)
          ? "value-large-red"
          : "value-large-green"
      }
    >
      {Util.calculateActualTotalSpending(selectedBudgets)} €
    </div>
  </div>
);

export default ActualTotalSpending;