import React from "react";
import Util from "../Util";

const BudgetCount = ({ selectedBudgets }) => (
  <div className="text-center col-6">
    <div className="top-note">BUDGET COUNT</div>
    <div className="value-large-black">
      {Util.getBudgetCount(selectedBudgets)}
    </div>
  </div>
);

export default BudgetCount;