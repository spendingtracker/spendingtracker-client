import React from "react";
import Util from "../Util.js";

const MonthCount = ({ periodTo, periodFrom }) => (
  <div className="text-center col-6">
    <div className="top-note">MONTH COUNT</div>
    <div className="value-large-black">
      {Util.getMonthCount(periodTo, periodFrom)}
    </div>
  </div>
);

export default MonthCount;