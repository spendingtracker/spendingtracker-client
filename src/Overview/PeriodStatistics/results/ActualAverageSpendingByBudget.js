import React from "react";
import Util from "../Util";

const ActualAverageSpendingByBudget = ({ selectedBudgets }) => (
  <div className="text-center col-6">
    <div className="top-note">ACTUAL</div>
    <div
      className={
        Util.calculateActualAverageSpendingByBudget(selectedBudgets) >
          Util.calculatePlannedAverageSpendingByBudget(selectedBudgets)
          ? "value-large-red"
          : "value-large-green"
      }
    >
      {Util.calculateActualAverageSpendingByBudget(selectedBudgets)} €
    </div>
  </div>
);

export default ActualAverageSpendingByBudget;