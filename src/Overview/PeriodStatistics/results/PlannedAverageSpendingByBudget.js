import React from "react";
import Util from "../Util";

const PlannedAverageSpendingByBudget = ({ selectedBudgets }) => (
  <div className="text-center col-6">
    <div className="top-note">PLANNED</div>
    <div className="value-large-grey">
      {Util.calculatePlannedAverageSpendingByBudget(selectedBudgets)} €
    </div>
  </div>
);

export default PlannedAverageSpendingByBudget;