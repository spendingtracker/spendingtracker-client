import React from "react";
import Util from "../Util";

const PlannedAverageSpendingByMonth = ({ selectedBudgets, periodTo, periodFrom }) => (
  <div className="text-center col-6">
    <div className="top-note">PLANNED</div>
    <div className="value-large-grey">
      {Util.calculatePlannedAverageSpendingByMonth(selectedBudgets, periodTo, periodFrom)} €
    </div>
  </div>
);

export default PlannedAverageSpendingByMonth;