import Constants from "../../Util/Constants";
import Helper from "../../Util/Helper";

function getPastPeriods() {
  return Helper.getPastPeriods(0).reverse();
}

function getMonthCount(periodTo, periodFrom) {
  if (periodTo == null || periodFrom == null) {
    return 0;
  }
  const monthTo = periodTo.periodStart;
  const monthFrom = periodFrom.periodStart;
  const monthDiff = monthTo.diff(monthFrom, "months");
  return monthDiff >= 0 ? monthDiff + 1 : 0;
}

function getBudgetCount(budgets) {
  return budgets.length > 0 ? budgets.length : 0;
}

function getSortedCategories(categories) {
  return categories.sort((a, b) =>
    a.deleted === b.deleted ? 0 : a.deleted ? 1 : -1
  );
}

function calculatePlannedTotalSpending(selectedBudgets) {
  return Helper.round(selectedBudgets.reduce((p, b) => p + b.volume, 0));
}

function calculateActualTotalSpending(selectedBudgets) {
  return Helper.round(
    selectedBudgets
      .flatMap((b) => b.transactions)
      .filter(
        (t) =>
          !t.internal &&
          t.amount > 0 &&
          t.status === Constants.TRANSACTION_STATUS.APPROVED
      )
      .reduce((a, t) => a + t.amount, 0)
  );
}

function calculatePlannedAverageSpendingByMonth(
  selectedBudgets,
  periodTo,
  periodFrom
) {
  const months = getMonthCount(periodTo, periodFrom);
  return selectedBudgets.length > 0
    ? Helper.round(selectedBudgets.reduce((p, b) => p + b.volume, 0) / months)
    : 0;
}

function calculateActualAverageSpendingByMonth(
  selectedBudgets,
  periodTo,
  periodFrom
) {
  const months = getMonthCount(periodTo, periodFrom);
  return selectedBudgets.length > 0
    ? Helper.round(
      selectedBudgets
        .flatMap((b) => b.transactions)
        .filter(
          (t) =>
            !t.internal &&
            t.amount > 0 &&
            t.status === Constants.TRANSACTION_STATUS.APPROVED
        )
        .reduce((a, t) => a + t.amount, 0) / months
    )
    : 0;
}

function calculatePlannedAverageSpendingByBudget(selectedBudgets) {
  return selectedBudgets.length > 0
    ? Helper.round(
      selectedBudgets.reduce((p, b) => p + b.volume, 0) /
      selectedBudgets.length
    )
    : 0;
}

function calculateActualAverageSpendingByBudget(selectedBudgets) {
  return selectedBudgets.length > 0
    ? Helper.round(
      selectedBudgets
        .flatMap((b) => b.transactions)
        .filter(
          (t) =>
            !t.internal &&
            t.amount > 0 &&
            t.status === Constants.TRANSACTION_STATUS.APPROVED
        )
        .reduce((a, t) => a + t.amount, 0) / selectedBudgets.length
    )
    : 0;
}

const Util = {
  getPastPeriods,
  getMonthCount,
  getBudgetCount,
  getSortedCategories,
  calculatePlannedTotalSpending,
  calculateActualTotalSpending,
  calculatePlannedAverageSpendingByMonth,
  calculateActualAverageSpendingByMonth,
  calculatePlannedAverageSpendingByBudget,
  calculateActualAverageSpendingByBudget,
};

export default Util;