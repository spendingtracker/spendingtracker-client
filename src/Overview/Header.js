import React from "react";
import GreyCheckbox from "../InputControls/GreyCheckbox";

const Header = ({
  spendingOnly,
  toggleSpendingOnly,
  includePending,
  toggleIncludePending,
  toggleSortByPlannedSpending,
  toggleSortByTotalSpending,
  toggleSortByTotalDeviation
}) => {
  return (
    <thead>
      <tr>
        <td align="left" colSpan="3">
          <GreyCheckbox
            checked={spendingOnly}
            onChange={toggleSpendingOnly}
            text="Spending only"
          />
        </td>
      </tr>
      <tr>
        <td align="left" colSpan="3">
          <GreyCheckbox
            checked={includePending}
            onChange={toggleIncludePending}
            text="Include pending"
          />
          <br />
        </td>
      </tr>
      <tr>
        <th align="left" width="34%">
          <div
            className="lightweight-note text-align-left"
            style={{ cursor: "pointer", margin: "10px" }}
            onClick={toggleSortByPlannedSpending}
          >
            Planned
          </div>
        </th>
        <th align="left" width="33%">
          <div
            className="lightweight-note text-align-left"
            style={{ cursor: "pointer", margin: "10px" }}
            onClick={toggleSortByTotalSpending}
          >
            Actual
          </div>
        </th>
        <th align="left" width="33%">
          <div
            className="lightweight-note text-align-left"
            style={{ cursor: "pointer", margin: "10px" }}
            onClick={toggleSortByTotalDeviation}
          >
            Balance
          </div>
        </th>
      </tr>
    </thead>
  );
};

export default Header;