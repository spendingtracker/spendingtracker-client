import React, { Component } from "react";
import { connect } from "react-redux";
import { IoMdCopy } from "react-icons/io";
import { Button, Input } from "reactstrap";
import Actions from "../redux/actions";
import Selectors from "../redux/selectors";
import Helper from "../Util/Helper";
import RestClient from "../Util/RestClient";
import Constants from "../Util/Constants";

class BudgetSetCloner extends Component {
  state = { budgets: [] };

  selectedMonthChanged = async (event) => {
    const monthOffset = event.target.value;
    let budgets = [];
    if (monthOffset !== "") {
      budgets = await RestClient.getBudgets(monthOffset);
    }
    this.setState({ budgets: budgets });
  };

  doClone = () => {
    const currentPeriod = Helper.getOffsetPeriod(this.props.monthOffset);
    const clonedBudgets = this.state.budgets.map((b) => ({
      year: currentPeriod.year,
      month: currentPeriod.month,
      volume: b.volume,
      trackingPeriod: b.trackingPeriod,
      category: b.category,
      transactions: b.transactions
        .filter((t) => t.template)
        .map((t) => ({
          ...t,
          status: Constants.TRANSACTION_STATUS.PENDING,
          time: Helper.currentTime_YYYYMMDDHHMMSS(),
        })),
    }));
    this.props.addBudgets(clonedBudgets);
  };

  renderMonthSelector() {
    return (
      <Input
        type="select"
        onChange={this.selectedMonthChanged}
        style={{ textAlignLast: "center" }}
        className="form-control"
      >
        <option key={""} value={""}></option>
        {Helper.getPastPeriods(this.props.monthOffset).map((p) => {
          return (
            <option key={p.text} value={p.monthOffset}>
              {p.text}
            </option>
          );
        })}
      </Input>
    );
  }

  renderPrototypeBudgetOverview() {
    return (
      <React.Fragment>
        <table className="table">
          <thead>
            <tr>
              <th
                width="65%"
                className="value-small-grey-strong text-align-left"
              >
                BUDGET
              </th>
              <th width="35%" className="text-align-right">
                <div className="value-small-grey-strong">PLANNED</div>
                <div className="value-small-grey">(ACTUAL)</div>
              </th>
            </tr>
          </thead>
          <tbody>
            {this.state.budgets.map((budget) => (
              <tr key={budget.id}>
                <td className="value-small-grey text-align-left">
                  {budget.category.name}
                </td>
                <td className="text-align-right">
                  <div className="value-small-grey-strong">
                    {Helper.round(budget.volume)} €
                  </div>
                  <div className="value-small-grey">
                    (
                    {Helper.round(
                      budget.transactions
                        .filter(
                          (t) =>
                            t.status ===
                              Constants.TRANSACTION_STATUS.APPROVED &&
                            !t.internal
                        )
                        .reduce((s, t) => s + t.amount, 0)
                    )}{" "}
                    €)
                  </div>
                </td>
              </tr>
            ))}
            <tr>
              <td className="value-small-grey-strong text-align-left">TOTAL</td>
              <td className="text-align-right">
                <div className="value-small-grey-strong">
                  {Helper.round(
                    this.state.budgets.reduce((s, b) => s + b.volume, 0)
                  )}{" "}
                  €
                </div>
                <div className="value-small-grey">
                  (
                  {Helper.round(
                    this.state.budgets
                      .flatMap((b) => b.transactions)
                      .filter(
                        (t) =>
                          t.status === Constants.TRANSACTION_STATUS.APPROVED &&
                          !t.internal
                      )
                      .reduce((s, t) => s + t.amount, 0)
                  )}{" "}
                  €)
                </div>
              </td>
            </tr>
          </tbody>
        </table>
        <div className="text-align-right">
          <Button onClick={this.doClone}>
            Clone <IoMdCopy />
          </Button>
        </div>
      </React.Fragment>
    );
  }

  renderInfoLabel() {
    return (
      <React.Fragment>
        <div className="value-small-grey">
          Select month with pre-existing budgets!
        </div>
      </React.Fragment>
    );
  }

  render() {
    return (
      <div className="menu-content">
        <div className="menu-content-item">
          <div className="value-small-grey text-align-center">
            NO BUDGETS FOR THE CURRENT MONTH
          </div>
          <br />
          <div className="text-align-left">
            <div className="top-note">COPY BUDGETS FROM</div>
          </div>
          {this.renderMonthSelector()}
          {this.state.budgets.length > 0
            ? this.renderPrototypeBudgetOverview()
            : this.renderInfoLabel()}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  monthOffset: Selectors.getMonthOffset(state),
});

const mapDispatchToProps = (dispatch) => ({
  addBudgets: (budget) => dispatch(Actions.addBudgets(budget)),
});

export default connect(mapStateToProps, mapDispatchToProps)(BudgetSetCloner);
