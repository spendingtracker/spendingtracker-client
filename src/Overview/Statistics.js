import React, { Component } from "react";
import { connect } from "react-redux";
import Actions from "../redux/actions";
import { MdKeyboardArrowDown, MdKeyboardArrowUp } from "react-icons/md";
import Helper from "../Util/Helper";
import StatisticsFactory from "../Util/StatisticsFactory";

class Statistics extends Component {
  state = { budgetRowsVisible: false };

  getBudgetStats() {
    return this.props.budgets
      .map(budget => {
        const plannedSpending = Helper.round(budget.volume);
        const totalSpending = Helper.round(
          StatisticsFactory.getTotalSpending(
            budget.transactions,
            this.props.spendingOnly,
            this.props.includePending
          )
        );
        const totalDeviation = Helper.round(plannedSpending - totalSpending);

        return {
          budget,
          plannedSpending,
          totalSpending,
          totalDeviation
        };
      })
      .sort((b1, b2) => {
        if (this.props.sortByPlannedSpendingAsc != null) {
          return this.props.sortByPlannedSpendingAsc
            ? b1.plannedSpending - b2.plannedSpending
            : b2.plannedSpending - b1.plannedSpending;
        } else {
          return 0;
        }
      })
      .sort((b1, b2) => {
        if (this.props.sortByTotalSpendingAsc != null) {
          return this.props.sortByTotalSpendingAsc
            ? b1.totalSpending - b2.totalSpending
            : b2.totalSpending - b1.totalSpending;
        } else {
          return 0;
        }
      })
      .sort((b1, b2) => {
        if (this.props.sortByTotalDeviationAsc != null) {
          return this.props.sortByTotalDeviationAsc
            ? b1.totalDeviation - b2.totalDeviation
            : b2.totalDeviation - b1.totalDeviation;
        } else {
          return 0;
        }
      });
  }

  renderBudgetStatistics = () => {
    return (
      <React.Fragment>
        {this.getBudgetStats().map(budgetStats => {
          return (
            <React.Fragment key={budgetStats.budget.id}>
              <tr>
                <td colSpan="3">
                  <div className="border-top text-align-left">
                    <button
                      className="btn btn-link"
                      onClick={() => this.props.selectAndDisplayBudget(budgetStats.budget.category.id)}>
                        {budgetStats.budget.category.name}
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td width="34%">
                  <div className="value-small-grey text-align-left">
                    {budgetStats.plannedSpending} €
                  </div>
                </td>
                <td width="33%">
                  <div className="value-small-grey text-align-left">
                    {budgetStats.totalSpending} €
                  </div>
                </td>
                <td width="33%">
                  <div className="text-align-left">
                    <span className={budgetStats.totalDeviation >= 0 ? "value-small-green" : "value-small-red"}>
                      {budgetStats.totalDeviation} €
                    </span>
                  </div>
                </td>
              </tr>
            </React.Fragment>
          );
        })}
      </React.Fragment>
    );
  };

  renderExpandButton = () => {
    return (
      <React.Fragment>
        <tr>
          <td
            colSpan="3"
            className="border-top text-align-center"
            style={{ cursor: "pointer" }}
            onClick={() =>
              this.setState({
                budgetRowsVisible: !this.state.budgetRowsVisible
              })
            }
          >
            {this.state.budgetRowsVisible ? (
              <MdKeyboardArrowUp />
            ) : (
              <MdKeyboardArrowDown />
            )}
          </td>
        </tr>
      </React.Fragment>
    );
  };

  render() {
    return (
      <React.Fragment>
        {this.renderExpandButton()}
        {this.state.budgetRowsVisible && this.renderBudgetStatistics()}
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  selectAndDisplayBudget: categoryId =>
    dispatch(dispatch => {
      dispatch(Actions.selectBudget(categoryId));
      dispatch(Actions.toggleMenu());
    })
});

export default connect(null, mapDispatchToProps)(Statistics);
