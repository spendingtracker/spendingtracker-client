import React, { Component } from "react";
import { Input } from "reactstrap";
import { connect } from "react-redux";
import Selectors from "../redux/selectors";
import Actions from "../redux/actions";

class CategorySelector extends Component {
  selectedCategoryChanged = event => {
    const categoryId = parseInt(event.target.value);
    this.props.selectBudget(categoryId);
  };

  render() {
    return (
      <Input
        type="select"
        onChange={this.selectedCategoryChanged}
        style={{ textAlignLast: "center" }}
        value={this.props.selectedCategory.id}
      >
        {this.props.categories
          .filter(
            c =>
              !c.deleted ||
              c.id === this.props.selectedCategory.id ||
              this.props.budgets.some(b => b.category.id === c.id)
          )
          .map(category => (
            <option key={category.id} value={category.id}>
              {category.name}
            </option>
          ))}
      </Input>
    );
  }
}

const mapStateToProps = state => ({
  categories: Selectors.getCategories(state),
  selectedCategory: Selectors.getSelectedCategory(state),
  budgets: Selectors.getBudgets(state)
});

const mapDispatchToProps = dispatch => ({
  selectBudget: categoryId => dispatch(Actions.selectBudget(categoryId))
});

export default connect(mapStateToProps, mapDispatchToProps)(CategorySelector);
