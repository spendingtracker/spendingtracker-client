import React, { Component } from "react";
import { IoMdHeart, IoMdLogOut } from "react-icons/io";
import { connect } from "react-redux";
import { Button } from "reactstrap";
import Overview from "../Overview";
import Categories from "../Categories";
import Actions from "../redux/actions";
import Selectors from "../redux/selectors";
import RestClient from "../Util/RestClient";

class Menu extends Component {
  state = { expiresInMinutes: RestClient.getExpiresInMinutes() };

  componentDidMount() {
    this.expiry = setInterval(
      () =>
        this.setState({ expiresInMinutes: RestClient.getExpiresInMinutes() }),
      60000
    );
  }

  render() {
    if (this.props.isMenuOpen) {
      return (
        <div className="menu-content">
          <div className="menu-content-item">
            <hr />
            <Overview />
            <hr />
            <Categories />
            <hr />
            <Button onClick={this.props.logout}>
              Log out <IoMdLogOut />
            </Button>
            <hr />
            <div className="lightweight-note">
              Authorization token:{" "}
              <strong>{RestClient.getToken().substring(7, 27) + "..."}</strong>
              <br />
              Expires in <strong>{this.state.expiresInMinutes}</strong> minutes
            </div>
            <hr />
            <div className="lightweight-note">
              Spendingtracker <br />
              Made with <IoMdHeart /> by Marek Lints <br />
            </div>
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}

const mapStateToProps = state => ({ isMenuOpen: Selectors.isMenuOpen(state) });

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(Actions.logout())
});

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
