import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from "@material-ui/core";
import React, { Component } from "react";
import { connect } from "react-redux";
import Selectors from "../redux/selectors";
import Category from "./Category";
import DeletedCategory from "./DeletedCategory";
import SystemCategory from "./SystemCategory";

class Categories extends Component {
  render() {
    return (
      <React.Fragment>
        <Accordion>
          <AccordionSummary>
            <div className="caption-1">Active Categories</div>
          </AccordionSummary>
          <AccordionDetails>
            <table width="100%">
              <tbody>
                {this.props.categories
                  .filter((c) => !c.deleted)
                  .map((category) => {
                    if (category.ownedByUser) {
                      return <Category key={category.id} category={category} />;
                    } else {
                      return (
                        <SystemCategory key={category.id} category={category} />
                      );
                    }
                  })}
                <tr>
                  <td colSpan="3">
                    <div className="caption-1 text-align-left">
                      Add New Category
                    </div>
                  </td>
                </tr>
                <Category />
              </tbody>
            </table>
          </AccordionDetails>
        </Accordion>
        {this.props.categories.some((c) => c.deleted) && (
          <Accordion>
            <AccordionSummary>
              <div className="caption-1">Deleted Categories</div>
            </AccordionSummary>
            <AccordionDetails>
              <table width="100%">
                <tbody>
                  {this.props.categories
                    .filter((c) => c.deleted)
                    .map((category) => (
                      <DeletedCategory key={category.id} category={category} />
                    ))}
                </tbody>
              </table>
            </AccordionDetails>
          </Accordion>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  categories: Selectors.getCategories(state),
});

export default connect(mapStateToProps, null)(Categories);
