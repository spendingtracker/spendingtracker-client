import React, { Component } from "react";
import { connect } from "react-redux";
import SmallUndoButton from "../InputControls/SmallUndoButton";
import Actions from "../redux/actions";
import ConfirmDialog from "../InputControls/ConfirmDialog";

class Category extends Component {
  state = {
    id: (this.props.category && this.props.category.id) || "",
    name: (this.props.category && this.props.category.name) || "",
    showUndo: false
  };

  componentDidUpdate(prevProps) {
    if (
      this.props.category &&
      (this.props.category.id !== prevProps.category.id ||
        this.props.category.name !== prevProps.category.name)
    ) {
      this.setState({
        id: this.props.category.id,
        name: this.props.category.name
      });
    }
  }

  handleUndo = () => {
    this.setState({ showUndo: true });
  };

  handleClose = () => {
    this.setState({ showUndo: false });
  };

  render() {
    return (
      <tr>
        <td colSpan="3">
          {!this.state.showUndo && (
            <React.Fragment>
              <div className="text-align-center">{this.state.name}</div>
              {this.renderButtons()}
            </React.Fragment>
          )}
          {this.renderConfirm()}
          <hr />
        </td>
      </tr>
    );
  }

  renderButtons() {
    return (
      <div
        className="text-align-center"
        style={{
          margin: "10px 0px 10px 0px"
        }}
      >
        <SmallUndoButton onClick={this.handleUndo} />
      </div>
    );
  }

  renderConfirm() {
    return (
      this.state.showUndo && (
        <div className="text-align-center">
          <ConfirmDialog
            visible={this.state.showUndo}
            color="warning"
            message={`Are you sure you want to restore the category '${this.state.name}'?`}
            handleClose={this.handleClose}
            handleAction={() => this.props.restoreCategory(this.state.id)}
          />
        </div>
      )
    );
  }
}

const mapDispatchToProps = dispatch => ({
  restoreCategory: categoryId => dispatch(Actions.restoreCategory(categoryId))
});

export default connect(
  null,
  mapDispatchToProps
)(Category);
