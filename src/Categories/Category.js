import React, { Component } from "react";
import { connect } from "react-redux";
import TextInput from "../InputControls/TextInput";
import SmallDeleteButton from "../InputControls/SmallDeleteButton";
import SmallSaveButton from "../InputControls/SmallSaveButton";
import Actions from "../redux/actions";
import ConfirmDialog from "../InputControls/ConfirmDialog";

class Category extends Component {
  state = {
    id: (this.props.category && this.props.category.id) || "",
    name: (this.props.category && this.props.category.name) || "",
    showDelete: false
  };

  componentDidUpdate(prevProps) {
    if (
      this.props.category &&
      (this.props.category.id !== prevProps.category.id ||
        this.props.category.name !== prevProps.category.name)
    ) {
      this.setState({
        id: this.props.category.id,
        name: this.props.category.name
      });
    }
  }

  handleDeleteClick = () => {
    this.setState({ showDelete: true });
  };

  handleCloseClick = () => {
    this.setState({ showDelete: false });
  };

  handleCategoryNameChange = name => this.setState({ name });

  handleSave = () => {
    if (this.state.name != null && this.state.name.trim() !== "") {
      let category = {
        id: this.state.id,
        name: this.state.name.trim()
      };
      this.props.saveCategory(category);

      if (!(category.id > 0)) {
        this.setState({ name: "" });
      }
    }
  };

  render() {
    return (
      <tr>
        <td colSpan="3">
          {!this.state.showDelete && (
            <React.Fragment>
              <div className="value-small-grey text-align-left">
                <TextInput
                  title="Your Category"
                  text={this.state.name}
                  onTextChange={this.handleCategoryNameChange}
                />
              </div>
              {this.renderButtons()}
            </React.Fragment>
          )}
          {this.renderConfirm()}
          <hr />
        </td>
      </tr>
    );
  }

  renderButtons() {
    return (
      <div
        className="text-align-center"
        style={{
          margin: "10px 0px 10px 0px"
        }}
      >
        <SmallSaveButton onClick={this.handleSave} />{" "}
        {this.state.id > 0 && (
          <SmallDeleteButton onClick={this.handleDeleteClick} />
        )}
      </div>
    );
  }

  renderConfirm() {
    return (
      this.state.showDelete && (
        <div className="text-align-center">
          <ConfirmDialog
            visible={this.state.showDelete}
            color="danger"
            message={`Are you sure you want to delete the category '${this.state.name}'?`}
            handleClose={this.handleCloseClick}
            handleAction={() => this.props.deleteCategory(this.state.id)}
          />
        </div>
      )
    );
  }
}

const mapDispatchToProps = dispatch => ({
  saveCategory: category => dispatch(Actions.saveCategory(category)),
  deleteCategory: categoryId => dispatch(Actions.deleteCategory(categoryId))
});

export default connect(
  null,
  mapDispatchToProps
)(Category);
