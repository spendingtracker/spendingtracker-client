import React from "react";

const SystemCategory = ({ category }) => (
  <tr>
    <td colSpan="3">
      <div className="text-align-left">
        <div className="top-note">System Category</div>
      </div>
      <div className="text-align-center">{category.name}</div>
      <hr />
    </td>
  </tr>
);

export default SystemCategory;