import React, { Component } from "react";
import { connect } from "react-redux";
import Actions from "./redux/actions";
import { IoIosPulse, IoMdLogIn } from "react-icons/io";
import { Form, Input, Button } from "reactstrap";

class Login extends Component {
  state = {
    username: "",
    password: ""
  };

  handleUsernameChange = event => {
    this.setState({ username: event.target.value });
  };

  handlePasswordChange = event => {
    this.setState({ password: event.target.value });
  };

  handleSubmit = event => {
    event.preventDefault();
    this.props.login(this.state);
  };

  validateForm = () => {
    return this.state.username.length > 0 && this.state.password.length > 0;
  };

  render() {
    return (
      <Form onSubmit={this.handleSubmit}>
        <div className="login-title">
          Spendingtracker <IoIosPulse />
        </div>
        <hr />
        <div className="login-subtitle">Log in</div>
        <Input
          type="text"
          value={this.state.username}
          onChange={this.handleUsernameChange}
          placeholder="Username"
          name="username"
          className="login-control"
        />
        <br />
        <Input
          type="password"
          value={this.state.password}
          onChange={this.handlePasswordChange}
          placeholder="Password"
          name="password"
          className="login-control"
        />
        <br />
        <Button
          disabled={!this.validateForm()}
          type="submit"
          className="login-control"
        >
          Login <IoMdLogIn />
        </Button>
        <hr />
        <br />
        <br />
      </Form>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  login: credentials => dispatch(Actions.login(credentials))
});

export default connect(
  null,
  mapDispatchToProps
)(Login);
