import React, { Component } from "react";
import { connect } from "react-redux";
import Selectors from "./redux/selectors";
import Actions from "./redux/actions";
import "./App.css";
import Login from "./Login";
import Tracker from "./Tracker";
import { Container, Row, Col } from "reactstrap";

class App extends Component {
  componentDidMount() {
    this.props.refreshAuthenticationStatus();
  }

  render() {
    return (
      <Container>
        <Row className="justify-content-center">
          <Col className="col-lg-6">
            {(this.props.authenticated && <Tracker />) || <Login />}
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  authenticated: Selectors.isAuthenticated(state)
});

const mapDispatchToProps = dispatch => ({
  refreshAuthenticationStatus: () =>
    dispatch(Actions.refreshAuthenticationStatus())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
