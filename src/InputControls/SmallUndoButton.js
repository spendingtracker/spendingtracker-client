import React from "react";
import { Button } from "reactstrap";
import { IoMdUndo } from "react-icons/io";

const SmallUndoButton = ({ id, onClick }) => (
  <Button id={id} onClick={onClick} size="sm">
    <IoMdUndo />
  </Button>
);

export default SmallUndoButton;