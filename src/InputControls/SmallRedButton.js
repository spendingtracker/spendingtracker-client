import React from "react";
import { Button } from "reactstrap";

const SmallRedButton = ({ id, text, onClick }) => (
  <Button
    style={{
      backgroundColor: "#9e0606",
      borderColor: "#9e0606"
    }}
    id={id}
    onClick={onClick}
    size="sm"
  >
    {text}
  </Button>
);

export default SmallRedButton;