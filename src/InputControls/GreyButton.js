import React from "react";
import { Button } from "reactstrap";

export default ({ id, text, onClick }) => (
  <Button id={id} onClick={onClick}>
    {text}
  </Button>
);
