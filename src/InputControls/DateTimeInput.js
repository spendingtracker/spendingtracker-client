import React, { Component } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import TimeInput from "./TimeInput";
import Helper from "../Util/Helper";

export default class DateTimeInput extends Component {
  constructor(props) {
    super(props);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleHourChange = this.handleHourChange.bind(this);
    this.handleMinuteChange = this.handleMinuteChange.bind(this);
  }

  handleDateChange(value) {
    const selectedDate = Helper.formatTime_YYYYMMDD(value);
    this.props.onTimeChange(selectedDate + "T" + this.getCurrentTime());
  }

  handleHourChange(event) {
    const selectedHour = event.target.value;
    this.props.onTimeChange(
      this.getCurrentDate() +
        "T" +
        selectedHour +
        ":" +
        this.getCurrentMinute() +
        ":" +
        this.getCurrentSecond()
    );
  }

  handleMinuteChange(event) {
    const selectedMinute = event.target.value;
    this.props.onTimeChange(
      this.getCurrentDate() +
        "T" +
        this.getCurrentHour() +
        ":" +
        selectedMinute +
        ":" +
        this.getCurrentSecond()
    );
  }

  render() {
    return (
      <div>
        <div className="top-note">{this.props.dateTitle}</div>
        <DatePicker
          selected={new Date(this.getCurrentDate())}
          onChange={this.handleDateChange}
          inline
        />
        <br />
        <TimeInput
          title={this.props.timeTitle}
          time={this.getCurrentTime()}
          onHourChange={this.handleHourChange}
          onMinuteChange={this.handleMinuteChange}
        />
      </div>
    );
  }

  getCurrentDate() {
    return this.props.dateTime.split("T")[0];
  }

  getCurrentTime() {
    return this.props.dateTime.split("T")[1];
  }

  getCurrentHour() {
    return this.getCurrentTime().split(":")[0];
  }

  getCurrentMinute() {
    return this.getCurrentTime().split(":")[1];
  }

  getCurrentSecond() {
    return this.getCurrentTime().split(":")[2];
  }
}
