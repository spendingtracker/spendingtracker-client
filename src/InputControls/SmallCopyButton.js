import React from "react";
import { Button } from "reactstrap";
import { IoIosCopy } from "react-icons/io";

const SmallCopyButton = ({ id, onClick }) => (
  <Button id={id} onClick={onClick} size="sm">
    <IoIosCopy />
  </Button>
);

export default SmallCopyButton;