import React from "react";
import { IoMdCheckbox, IoMdSquareOutline } from "react-icons/io";

const GreyCheckbox = ({ checked, onChange, text }) => {
  return (
    <table className="light-box">
      <tbody>
        <tr>
          <td style={{ cursor: "pointer" }} onClick={() => onChange(!checked)}>
            {!checked ? <IoMdSquareOutline /> : <IoMdCheckbox />} {text}
          </td>
        </tr>
      </tbody>
    </table>
  );
};

export default GreyCheckbox;