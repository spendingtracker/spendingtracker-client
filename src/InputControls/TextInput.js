import React, { Component } from "react";
import { Input } from "reactstrap";

export default class TextInput extends Component {
  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    this.props.onTextChange(event.target.value);
  }

  render() {
    const text = this.props.text != null ? this.props.text : "";
    return (
      <div>
        <div className="top-note">{this.props.title}</div>
        <div>
          <Input
            id={this.props.id}
            type="text"
            value={text}
            onChange={this.handleInputChange}
            placeholder="Description..."
            style={{ textAlign: "center" }}
          />
        </div>
      </div>
    );
  }
}
