import React from "react";
import { Button } from "reactstrap";
import { IoIosCheckmarkCircle } from "react-icons/io";

const SmallApproveButton = ({ id, onClick }) => (
  <Button
    id={id}
    onClick={onClick}
    size="sm"
    style={{
      backgroundColor: "#006600",
      borderColor: "#006600"
    }}
  >
    <IoIosCheckmarkCircle />
  </Button>
);

export default SmallApproveButton;