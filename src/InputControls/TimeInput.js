import React from "react";

const TimeInput = ({ title, time, onHourChange, onMinuteChange }) => {
  const currentHour = time.split(":")[0];
  const currentMinute = time.split(":")[1];
  return (
    <div>
      <div className="top-note">{title}</div>
      <table style={{ width: "100%" }}>
        <tbody>
          <tr>
            <td>
              <select
                className="form-control"
                style={{ textAlignLast: "center" }}
                onChange={onHourChange}
                value={currentHour}
              >
                {Array.from(Array(24).keys()).map(hour => (
                  <option key={hour}>{hour.toString().padStart(2, "0")}</option>
                ))}
              </select>
            </td>
            <td>{" : "}</td>
            <td>
              <select
                className="form-control"
                style={{ textAlignLast: "center" }}
                onChange={onMinuteChange}
                value={currentMinute}
              >
                {Array.from(Array(60).keys()).map(min => (
                  <option key={min}>{min.toString().padStart(2, "0")}</option>
                ))}
              </select>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default TimeInput;