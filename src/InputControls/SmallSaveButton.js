import React from "react";
import { Button } from "reactstrap";
import { IoMdSave } from "react-icons/io";

const SmallSaveButton = ({ id, onClick }) => (
  <Button id={id} onClick={onClick} size="sm">
    <IoMdSave />
  </Button>
);

export default SmallSaveButton;