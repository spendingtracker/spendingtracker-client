import React from "react";
import { Button } from "reactstrap";
import { IoIosCreate } from "react-icons/io";

const SmallEditButton = ({ id, onClick }) => (
  <Button id={id} onClick={onClick} size="sm">
    <IoIosCreate />
  </Button>
);

export default SmallEditButton;