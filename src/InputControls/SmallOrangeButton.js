import React from "react";
import { Button } from "reactstrap";

const SmallOrangeButton = ({ id, text, onClick }) => (
  <Button color="warning" id={id} onClick={onClick} size="sm">
    {text}
  </Button>
);

export default SmallOrangeButton;