import React from "react";
import { Button } from "reactstrap";
import { IoMdTrash } from "react-icons/io";

const SmallDeleteButton = ({ id, onClick }) => (
  <Button
    style={{
      backgroundColor: "#9e0606",
      borderColor: "#9e0606"
    }}
    id={id}
    onClick={onClick}
    size="sm"
  >
    <IoMdTrash />
  </Button>
);

export default SmallDeleteButton;