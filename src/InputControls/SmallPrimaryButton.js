import React from "react";
import { Button } from "reactstrap";

const SmallPrimaryButton = ({ id, text, onClick }) => (
  <Button color="primary" id={id} onClick={onClick} size="sm">
    {text}
  </Button>
);

export default SmallPrimaryButton;