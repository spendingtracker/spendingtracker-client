import React from "react";
import { Button } from "reactstrap";
import { IoMdShuffle } from "react-icons/io";

const SmallReverseButton = ({ id, onClick }) => (
  <Button id={id} onClick={onClick} size="sm">
    <IoMdShuffle />
  </Button>
);

export default SmallReverseButton;