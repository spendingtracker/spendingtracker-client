import React, { Component } from "react";
import { Alert } from "reactstrap";
import SmallGreyButton from "./SmallGreyButton";
import SmallRedButton from "./SmallRedButton";
import SmallOrangeButton from "./SmallOrangeButton";
import SmallPrimaryButton from "./SmallPrimaryButton";

export default class ConfirmDialog extends Component {
  handleAction = () => {
    this.props.handleAction();
    this.props.handleClose();
  };

  render() {
    if (!this.props.visible) {
      return null;
    } else {
      return (
        <Alert color={this.props.color}>
          {this.props.message}
          <div>
            <SmallGreyButton text="Cancel" onClick={this.props.handleClose} />{" "}
            {this.props.color === "primary" && (
              <SmallPrimaryButton text="Confirm" onClick={this.handleAction} />
            )}
            {this.props.color === "danger" && (
              <SmallRedButton text="Confirm" onClick={this.handleAction} />
            )}
            {this.props.color === "warning" && (
              <SmallOrangeButton text="Confirm" onClick={this.handleAction} />
            )}
          </div>
        </Alert>
      );
    }
  }
}
