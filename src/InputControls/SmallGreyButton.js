import React from "react";
import { Button } from "reactstrap";

const SmallGreyButton = ({ id, text, onClick }) => (
  <Button id={id} onClick={onClick} size="sm">
    {text}
  </Button>
);

export default SmallGreyButton;