import React, { Component } from "react";
import { Input } from "reactstrap";
import Helper from "../Util/Helper";

export default class MoneyInput extends Component {
  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    let amountStr = Helper.removeEuroSymbolFromPriceSubstr(event.target.value);
    if (Helper.isPriceSubstr(amountStr)) {
      this.props.onValueChange(amountStr);
    }
  }

  render() {
    const amount = this.props.amount !== "" ? "€" + this.props.amount : "";
    return (
      <div>
        <div className="top-note">{this.props.title}</div>
        <div>
          <Input
            id={this.props.id}
            type="text"
            value={amount}
            onChange={this.handleInputChange}
            placeholder="€0.00"
            style={{ textAlign: "center" }}
          />
        </div>
      </div>
    );
  }
}
