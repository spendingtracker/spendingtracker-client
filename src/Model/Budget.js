import Helper from "../Util/Helper";

export default class Budget {
  constructor({
    id,
    year,
    month,
    volume,
    dailyVolume,
    trackingPeriod,
    category,
    transactions
  }) {
    this.id = parseInt(id);
    this.year = parseInt(year);
    this.month = parseInt(month);
    this.volume = Helper.round(parseFloat(volume));
    this.dailyVolume = Helper.round(parseFloat(dailyVolume));
    this.trackingPeriod = trackingPeriod;
    this.category = category;
    this.transactions = transactions;
  }
}
