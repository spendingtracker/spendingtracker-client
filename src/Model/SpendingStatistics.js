import Helper from "../Util/Helper";

export default class SpendingStatistics {
  constructor({
    totalSpending,
    dailySpending,
    totalDeviation,
    dailyDeviation,
    totalRemainingVolume,
    dailyRemainingVolume,
    todaysSpending,
    transactionCount,
    todaysTransactionCount
  }) {
    this.totalSpending = Helper.round(totalSpending);
    this.dailySpending = Helper.round(dailySpending);
    this.totalDeviation = Helper.round(totalDeviation);
    this.dailyDeviation = Helper.round(dailyDeviation);
    this.totalRemainingVolume = Helper.round(totalRemainingVolume);
    this.dailyRemainingVolume = Helper.round(dailyRemainingVolume);
    this.todaysSpending = Helper.round(todaysSpending);
    this.transactionCount = parseInt(transactionCount);
    this.todaysTransactionCount = parseInt(todaysTransactionCount);
  }
}
