import moment from "moment";
import React, { Component } from "react";
import { IoMdArrowRoundDown, IoMdArrowRoundUp } from "react-icons/io";
import { connect } from "react-redux";
import { Button, ButtonGroup, Form } from "reactstrap";
import GreyCheckbox from "../InputControls/GreyCheckbox";
import MoneyInput from "../InputControls/MoneyInput";
import TextInput from "../InputControls/TextInput";
import Actions from "../redux/actions";
import Selectors from "../redux/selectors";
import Constants from "../Util/Constants";

class AddTransaction extends Component {
  state = { amountValue: "", comment: "", pending: false };

  handleAmountChange = (amount) => this.setState({ amountValue: amount });

  handleCommentChange = (comment) => this.setState({ comment: comment });

  togglePending = (pending) => this.setState({ pending: pending });

  handleSubmit = (event) => {
    event.preventDefault();
    let amount = this.state.amountValue;
    if (amount > 0 && amount < 10000) {
      if (event.target.id === "layUpButton") {
        amount = amount * -1;
      }
      this.props.postTransaction({
        time: moment(new Date()).format("YYYY-MM-DDTHH:mm:ss"),
        amount: amount,
        comment: this.state.comment.trim(),
        status: this.state.pending
          ? Constants.TRANSACTION_STATUS.PENDING
          : Constants.TRANSACTION_STATUS.APPROVED,
        budgetId: this.props.selectedBudget.id,
      });
      this.setState({ amountValue: "", comment: "", pending: false });
    }
  };

  render() {
    if (this.props.selectedBudget.id > 0) {
      return (
        <Form className="form-row-content">
          <MoneyInput
            id="amount"
            title="Amount"
            amount={this.state.amountValue}
            onValueChange={this.handleAmountChange}
          />
          <br />
          <TextInput
            id="comment"
            title="Comment"
            text={this.state.comment}
            onTextChange={this.handleCommentChange}
          />
          <br />
          <GreyCheckbox
            checked={this.state.pending}
            onChange={this.togglePending}
            text="Pending"
          />
          <br />
          <div className="text-center">
            <ButtonGroup>
              <Button id="spendButton" onClick={this.handleSubmit}>
                <IoMdArrowRoundDown /> Spend
              </Button>
              <Button id="layUpButton" onClick={this.handleSubmit}>
                <IoMdArrowRoundUp /> Lay up
              </Button>
            </ButtonGroup>
          </div>
        </Form>
      );
    } else {
      return (
        <div className="value-small-grey text-center form-row-content">
          NO BUDGET DEFINED
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  selectedBudget: Selectors.getSelectedBudget(state),
});

const mapDispatchToProps = (dispatch) => ({
  postTransaction: (transaction) =>
    dispatch(Actions.postTransaction(transaction)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddTransaction);
