import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from "@material-ui/core";
import Transactions from "./Transactions";
import AddTransaction from "./AddTransaction";
import Balance from "./Balance";
import Header from "./Header";
import CategorySelector from "./CategorySelector";
import Menu from "./Menu";
import Selectors from "./redux/selectors";
import Actions from "./redux/actions";

class Tracker extends Component {
  componentDidMount() {
    this.props.fetchCategories();
  }

  render() {
    const content = (
      <div style={{ display: this.props.isMenuOpen ? "none" : "block" }}>
        <CategorySelector />
        <br />
        <Accordion>
          <AccordionSummary>
            <div className="caption-1">New Transaction</div>
          </AccordionSummary>
          <AccordionDetails>
            <AddTransaction />
          </AccordionDetails>
        </Accordion>
        <Accordion>
          <AccordionSummary>
            <div className="caption-1">Transactions</div>
          </AccordionSummary>
          <AccordionDetails>
            <Transactions />
          </AccordionDetails>
        </Accordion>
        <Accordion>
          <AccordionSummary>
            <div className="caption-1">Balance</div>
          </AccordionSummary>
          <AccordionDetails>
            <Balance />
          </AccordionDetails>
        </Accordion>
      </div>
    );

    return (
      <div>
        <Header />
        <Menu />
        {content}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  isMenuOpen: Selectors.isMenuOpen(state),
});

const mapDispatchToProps = (dispatch) => ({
  fetchCategories: () => dispatch(Actions.fetchCategories()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Tracker);
