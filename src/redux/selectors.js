const isAuthenticated = state => state.appStatus.authenticated;

const isMenuOpen = state => state.appStatus.menuOpen;

const getMonthOffset = state => state.budgets.monthOffset;

const getBudgets = state => state.budgets.budgets;

const getCategories = state => state.budgets.categories;

const getSelectedCategory = state => state.budgets.selectedCategory;

const getSelectedBudget = state => state.budgets.selectedBudget;

const selectors = {
  isAuthenticated,
  isMenuOpen,
  getMonthOffset,
  getBudgets,
  getSelectedBudget,
  getCategories,
  getSelectedCategory
};

export default selectors;