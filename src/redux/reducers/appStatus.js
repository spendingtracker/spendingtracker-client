import ActionTypes from "../actionTypes";

const initialState = {
  authenticated: false,
  menuOpen: false
};

const appStatus = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.REFRESH_AUTHENTICATION_STATUS: {
      return {
        ...state,
        authenticated: action.authenticated,
        menuOpen: action.authenticated ? state.menuOpen : false
      };
    }
    case ActionTypes.TOGGLE_MENU: {
      return {
        ...state,
        menuOpen: !state.menuOpen
      };
    }
    default:
      return state;
  }
};

export default appStatus;