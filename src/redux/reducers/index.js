import { combineReducers } from "redux";
import appStatus from "./appStatus";
import budgets from "./budgets";

export default combineReducers({ appStatus, budgets });
