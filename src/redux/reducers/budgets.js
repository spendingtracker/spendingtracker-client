import ActionTypes from "../actionTypes";

const initialState = {
  monthOffset: 0,
  categories: [],
  selectedCategory: {},
  budgets: [],
  selectedBudget: {},
};

const budgets = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.RECEIVE_BUDGETS: {
      const monthOffset = action.monthOffset;
      const categories =
        action.categories.length > 0 ? action.categories : state.categories;
      const selectedCategory = state.selectedCategory.id
        ? state.selectedCategory
        : categories.find((category) => !!category);
      const budgets = action.budgets;
      const selectedBudget =
        action.budgets.find(
          (budget) => budget.category.id === selectedCategory.id
        ) || {};

      return {
        ...state,
        monthOffset,
        categories,
        selectedCategory,
        budgets,
        selectedBudget,
      };
    }
    case ActionTypes.SELECT_BUDGET: {
      const selectedBudget =
        state.budgets.find(
          (budget) => budget.category.id === action.categoryId
        ) || {};
      const selectedCategory =
        state.categories.find(
          (category) => category.id === action.categoryId
        ) || {};

      return {
        ...state,
        selectedBudget,
        selectedCategory,
      };
    }
    default:
      return state;
  }
};

export default budgets;