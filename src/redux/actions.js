import BudgetFactory from "../Util/BudgetFactory";
import RestClient from "../Util/RestClient";
import ActionTypes from "./actionTypes";

const login = credentials => {
  return dispatch =>
    RestClient.login(credentials).finally(() =>
      dispatch(refreshAuthenticationStatus())
    );
};

const logout = () => {
  return dispatch => {
    RestClient.clearToken();
    dispatch(refreshAuthenticationStatus());
  };
};

const refreshAuthenticationStatus = () => {
  return {
    type: ActionTypes.REFRESH_AUTHENTICATION_STATUS,
    authenticated: RestClient.isAuthenticated()
  };
};

const toggleMenu = () => ({ type: ActionTypes.TOGGLE_MENU });

const increaseMonthOffset = () => {
  return (dispatch, getState) => {
    dispatch(fetchBudgets(getState().budgets.monthOffset + 1));
  };
};

const decreaseMonthOffset = () => {
  return (dispatch, getState) => {
    dispatch(fetchBudgets(getState().budgets.monthOffset - 1));
  };
};

const receiveBudgets = (monthOffset, categories, budgets) => {
  return {
    type: ActionTypes.RECEIVE_BUDGETS,
    monthOffset: monthOffset,
    categories: categories,
    budgets: BudgetFactory.createBudgets(budgets)
  };
};

const fetchBudgets = (monthOffset, categories = []) => {
  return dispatch => {
    RestClient.getBudgets(monthOffset)
      .then(budgets => {
        dispatch(receiveBudgets(monthOffset, categories, budgets));
      })
      .finally(() => dispatch(refreshAuthenticationStatus()));
  };
};

const saveBudget = budget => {
  return (dispatch, getState) => {
    if (budget.id > 0) {
      RestClient.putBudget(budget)
        .then(() => dispatch(fetchBudgets(getState().budgets.monthOffset)))
        .finally(() => dispatch(refreshAuthenticationStatus()));
    } else {
      RestClient.postBudget(budget)
        .then(() => dispatch(fetchBudgets(getState().budgets.monthOffset)))
        .finally(() => dispatch(refreshAuthenticationStatus()));
    }
  };
};

const addBudgets = budgets => {
  return (dispatch, getState) => {
    RestClient.postBudgets(budgets)
      .then(() => dispatch(fetchBudgets(getState().budgets.monthOffset)))
      .finally(() => dispatch(refreshAuthenticationStatus()));
  };
};

const fetchCategories = () => {
  return (dispatch, getState) => {
    RestClient.getCategories()
      .then(categories => {
        dispatch(fetchBudgets(getState().budgets.monthOffset, categories));
      })
      .finally(() => dispatch(refreshAuthenticationStatus()));
  };
};

const saveCategory = category => {
  return dispatch => {
    if (category.id > 0) {
      RestClient.putCategory(category)
        .then(() => dispatch(fetchCategories()))
        .finally(() => dispatch(refreshAuthenticationStatus()));
    } else {
      RestClient.postCategory(category)
        .then(() => dispatch(fetchCategories()))
        .finally(() => dispatch(refreshAuthenticationStatus()));
    }
  };
};

const deleteCategory = categoryId => {
  return dispatch => {
    RestClient.deleteCategory(categoryId)
      .then(() => dispatch(fetchCategories()))
      .finally(() => dispatch(refreshAuthenticationStatus()));
  };
};

const restoreCategory = categoryId => {
  return dispatch => {
    RestClient.restoreCategory(categoryId)
      .then(() => dispatch(fetchCategories()))
      .finally(() => dispatch(refreshAuthenticationStatus()));
  };
};

const selectBudget = categoryId => ({
  type: ActionTypes.SELECT_BUDGET,
  categoryId: categoryId
});

const postTransaction = transaction => {
  return (dispatch, getState) => {
    RestClient.postTransaction(transaction)
      .then(() => dispatch(fetchBudgets(getState().budgets.monthOffset)))
      .finally(() => dispatch(refreshAuthenticationStatus()));
  };
};

const postTransactions = transactions => {
  return (dispatch, getState) => {
    RestClient.postTransactions(transactions)
      .then(() => dispatch(fetchBudgets(getState().budgets.monthOffset)))
      .finally(() => dispatch(refreshAuthenticationStatus()));
  };
};

const putTransaction = transaction => {
  return (dispatch, getState) => {
    RestClient.putTransaction(transaction)
      .then(() => dispatch(fetchBudgets(getState().budgets.monthOffset)))
      .finally(() => dispatch(refreshAuthenticationStatus()));
  };
};

const deleteTransaction = transactionId => {
  return (dispatch, getState) => {
    RestClient.deleteTransaction(transactionId)
      .then(() => dispatch(fetchBudgets(getState().budgets.monthOffset)))
      .finally(() => dispatch(refreshAuthenticationStatus()));
  };
};

const actions = {
  login,
  logout,
  refreshAuthenticationStatus,
  toggleMenu,
  increaseMonthOffset,
  decreaseMonthOffset,
  saveBudget,
  addBudgets,
  fetchCategories,
  saveCategory,
  deleteCategory,
  restoreCategory,
  selectBudget,
  postTransaction,
  postTransactions,
  putTransaction,
  deleteTransaction
};

export default actions;