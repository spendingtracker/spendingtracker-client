const REFRESH_AUTHENTICATION_STATUS = "REFRESH_AUTHENTICATION_STATUS";
const TOGGLE_MENU = "TOGGLE_MENU";
const RECEIVE_BUDGETS = "RECEIVE_BUDGETS";
const SELECT_BUDGET = "SELECT_BUDGET";
const SAVE_BUDGET = "SAVE_BUDGET";

const actionTypes = {
  REFRESH_AUTHENTICATION_STATUS,
  TOGGLE_MENU,
  RECEIVE_BUDGETS,
  SELECT_BUDGET,
  SAVE_BUDGET
};

export default actionTypes;