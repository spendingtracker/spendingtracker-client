import moment from "moment";
import React, { Component } from "react";
import {
  IoMdArrowRoundDown,
  IoMdArrowRoundUp,
  IoMdDocument
} from "react-icons/io";
import { connect } from "react-redux";
import ConfirmDialog from "../InputControls/ConfirmDialog";
import SmallApproveButton from "../InputControls/SmallApproveButton";
import SmallCopyButton from "../InputControls/SmallCopyButton";
import SmallDeleteButton from "../InputControls/SmallDeleteButton";
import SmallEditButton from "../InputControls/SmallEditButton";
import SmallReverseButton from "../InputControls/SmallReverseButton";
import Actions from "../redux/actions";
import Constants from "../Util/Constants";
import Helper from "../Util/Helper";
import EditTransaction from "./EditTransaction";

class Transaction extends Component {
  state = {
    showView: true,
    showButtons: true,
    showEdit: false,
    showCopy: false,
    showReverse: false,
    showDelete: false,
  };

  handleApproveClick = () => {
    this.props.putTransaction({
      ...this.props.transaction,
      time: Helper.currentTime_YYYYMMDDHHMMSS(),
      status: Constants.TRANSACTION_STATUS.APPROVED,
    });
  };

  handleEditClick = () => {
    this.setState({ showView: false, showButtons: false, showEdit: true });
  };

  handleCopyClick = () => {
    this.setState({ showButtons: false, showCopy: true });
  };

  handleReverseClick = () => {
    this.setState({ showButtons: false, showReverse: true });
  };

  handleDeleteClick = () => {
    this.setState({ showButtons: false, showDelete: true });
  };

  handleCloseClick = () => {
    this.setState({
      showView: true,
      showButtons: true,
      showEdit: false,
      showCopy: false,
      showReverse: false,
      showDelete: false,
    });
  };

  renderView() {
    return (
      this.state.showView && (
        <div>
          <div
            style={{
              display: "flex",
            }}
          >
            <div
              style={{
                flex: 1,
                display: "flex",
                justifyContent: "left",
                alignItems: "flexStart",
              }}
            >
              {this.props.transaction.template && <IoMdDocument />}
            </div>
            <div
              style={{
                flex: 3,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <h3
                className={
                  this.props.transaction.amount > 0
                    ? "value-negative"
                    : "value-positive"
                }
              >
                €{Math.abs(this.props.transaction.amount)}
                {this.props.transaction.amount >= 0 ? (
                  <IoMdArrowRoundDown />
                ) : (
                  <IoMdArrowRoundUp />
                )}
              </h3>
            </div>
            <div style={{ flex: 1 }}></div>
          </div>
          {Helper.formatTime_HHMM(this.props.transaction.time)}
          <p>{this.props.transaction.comment}</p>
        </div>
      )
    );
  }

  renderButtonBar() {
    return (
      this.state.showButtons && (
        <div className="text-center">
          {this.props.transaction.status ===
            Constants.TRANSACTION_STATUS.PENDING && (
              <SmallApproveButton
                id="approveTransaction"
                onClick={this.handleApproveClick}
              />
            )}{" "}
          <SmallEditButton
            id="editTransaction"
            onClick={this.handleEditClick}
          />{" "}
          <SmallCopyButton
            id="copyTransaction"
            onClick={this.handleCopyClick}
          />{" "}
          <SmallReverseButton
            id="reverseTransaction"
            onClick={this.handleReverseClick}
          />{" "}
          <SmallDeleteButton
            id="deleteTransaction"
            onClick={this.handleDeleteClick}
          />
        </div>
      )
    );
  }

  renderEdit() {
    return (
      this.state.showEdit && (
        <EditTransaction
          onClick={this.handleCloseClick}
          transaction={this.props.transaction}
        />
      )
    );
  }

  renderConfirmDialogs() {
    return (
      <React.Fragment>
        <ConfirmDialog
          visible={this.state.showCopy}
          color="primary"
          message="Are you sure you want to clone the transaction?"
          handleClose={this.handleCloseClick}
          handleAction={() => {
            this.props.postTransaction({
              ...this.props.transaction,
              id: null,
              time: moment(new Date()).format("YYYY-MM-DDTHH:mm:ss"),
            });
          }}
        />
        <ConfirmDialog
          visible={this.state.showReverse}
          color="warning"
          message="Are you sure you want to reverse the transaction?"
          handleClose={this.handleCloseClick}
          handleAction={() => {
            this.props.putTransaction({
              ...this.props.transaction,
              amount: this.props.transaction.amount * -1,
            });
          }}
        />
        <ConfirmDialog
          visible={this.state.showDelete}
          color="danger"
          message="Are you sure you want to delete the transaction?"
          handleClose={this.handleCloseClick}
          handleAction={() =>
            this.props.deleteTransaction(this.props.transaction.id)
          }
        />
      </React.Fragment>
    );
  }

  render() {
    return (
      <React.Fragment>
        <div
          className="text-center"
          style={{
            background:
              this.props.transaction.status ===
                Constants.TRANSACTION_STATUS.PENDING
                ? "#EAEDED"
                : "#D6EAF8",
            borderRadius: "2px",
            padding: "10px 6px 10px 6px",
            margin: "10px 0px 0px 0px",
          }}
        >
          {this.renderView()}
          {this.renderButtonBar()}
          {this.renderEdit()}
          {this.renderConfirmDialogs()}
        </div>
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  postTransaction: (transaction) =>
    dispatch(Actions.postTransaction(transaction)),
  putTransaction: (transaction) =>
    dispatch(Actions.putTransaction(transaction)),
  deleteTransaction: (transactionId) =>
    dispatch(Actions.deleteTransaction(transactionId)),
});

export default connect(null, mapDispatchToProps)(Transaction);
