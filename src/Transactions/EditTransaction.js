import React, { Component } from "react";
import { IoMdClose, IoMdSave } from "react-icons/io";
import { connect } from "react-redux";
import { Button, ButtonGroup } from "reactstrap";
import DateTimeInput from "../InputControls/DateTimeInput";
import GreyCheckbox from "../InputControls/GreyCheckbox";
import MoneyInput from "../InputControls/MoneyInput";
import TextInput from "../InputControls/TextInput";
import Actions from "../redux/actions";
import Constants from "../Util/Constants";

class EditTransaction extends Component {
  state = {
    amountValue: Math.abs(this.props.transaction.amount),
    comment: this.props.transaction.comment,
    pending:
      this.props.transaction.status === Constants.TRANSACTION_STATUS.PENDING
        ? true
        : false,
    template: this.props.transaction.template,
    transactionTime: this.props.transaction.time,
    type: this.props.transaction.amount > 0 ? "SPENDING" : "LAYUP",
  };

  handleAmountChange = (amount) => this.setState({ amountValue: amount });

  handleCommentChange = (comment) => this.setState({ comment: comment });

  togglePending = (pending) => this.setState({ pending: pending });

  toggleTemplate = (template) => this.setState({ template: template });

  handleDateTimeChange = (dateTime) =>
    this.setState({ transactionTime: dateTime });

  save = () => {
    let amount = this.state.amountValue;
    const transactionTime = this.state.transactionTime;
    if (amount > 0 && amount < 10000 && transactionTime != null) {
      if (this.state.type === "LAYUP") {
        amount = amount * -1;
      }
      this.props.putTransaction({
        id: this.props.transaction.id,
        time: transactionTime,
        amount: amount,
        comment: this.state.comment,
        status: this.state.pending
          ? Constants.TRANSACTION_STATUS.PENDING
          : Constants.TRANSACTION_STATUS.APPROVED,
        template: this.state.template,
        budgetId: this.props.transaction.budgetId,
      });
      this.props.onClick();
    }
  };

  render() {
    return (
      <div className="dotted-box">
        <MoneyInput
          id="amount"
          title="Amount"
          amount={this.state.amountValue}
          onValueChange={this.handleAmountChange}
        />
        <br />
        <TextInput
          id="comment"
          title="Comment"
          text={this.state.comment}
          onTextChange={this.handleCommentChange}
        />
        <br />
        <DateTimeInput
          id="time"
          dateTitle="Date"
          timeTitle="Time"
          dateTime={this.state.transactionTime}
          onTimeChange={this.handleDateTimeChange}
        />
        <br />
        <GreyCheckbox
          checked={this.state.pending}
          onChange={this.togglePending}
          text="Pending"
        />
        <br />
        <GreyCheckbox
          checked={this.state.template}
          onChange={this.toggleTemplate}
          text="Template"
        />
        <br />
        <ButtonGroup>
          <Button id="cancel" onClick={this.props.onClick}>
            <IoMdClose /> Cancel
          </Button>
          <Button id="save" text="Save" onClick={this.save}>
            <IoMdSave /> Save
          </Button>
        </ButtonGroup>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  putTransaction: (transaction) =>
    dispatch(Actions.putTransaction(transaction)),
});

export default connect(null, mapDispatchToProps)(EditTransaction);
