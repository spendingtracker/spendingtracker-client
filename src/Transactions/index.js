import React, { Component } from "react";
import { connect } from "react-redux";
import Selectors from "../redux/selectors";
import Helper from "../Util/Helper";
import Transaction from "./Transaction";

class Transactions extends Component {

  render() {
    if (!this.props.selectedBudget.transactions || this.props.selectedBudget.transactions.length === 0) {
      return (
        <div className="value-small-grey text-center form-row-content">
          NO SPENDINGS
        </div>
      )
    }
    const groupedTxSet = this.groupByDate(this.props.selectedBudget.transactions)
    return (
      <div className="form-row-content">
        {groupedTxSet.map(groupedTxs => {
          return (
            <div key={groupedTxs.txDate} style={{
              border: "1px solid #EAEDED",
              borderRadius: "2px",
              padding: "6px",
              margin: "6px 0px 10px 0px"
            }}>
              <div className="caption-1 text-align-center">
                <strong>{Helper.formatTime_DDMMYYYY(groupedTxs.txDate)}</strong>&nbsp;-&nbsp;
                €{Helper.round(groupedTxs.tx.reduce((acc, tx) => acc + tx.amount, 0))}
              </div>
              {groupedTxs.tx.map(tx => (
                <Transaction key={tx.id} transaction={tx} />
              ))}
            </div>
          )
        }
        )}
      </div>
    )
  }

  groupByDate(transactions) {
    if (!transactions) {
      return {}
    }
    return Object.entries(
      transactions.reduce((acc, tx) => {
        const txDate = tx.time.substr(0, 10)
        if (!acc[txDate]) {
          acc[txDate] = []
        }
        acc[txDate].push(tx)
        return acc
      }, {})
    ).map(([txDate, tx]) => ({ txDate, tx }))
  }
}

const mapStateToProps = (state) => ({
  selectedBudget: Selectors.getSelectedBudget(state),
});

export default connect(mapStateToProps, null)(Transactions);
